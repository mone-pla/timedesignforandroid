package jp.co.monepla.timedesign.view.activity


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import jp.co.monepla.timedesign.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class LoginActivityTest2 {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(LoginActivity::class.java)

    @Test
    fun loginActivityTest2() {
        val appCompatButton = onView(
                allOf(withId(R.id.skip), withText("skip"))).inRoot(RootMatchers.isDialog())
        appCompatButton.perform(click())

        val appCompatEditText1 = onView(
                withId(R.id.fieldEmail)).inRoot(RootMatchers.withDecorView(`is`(mActivityTestRule.activity.window.decorView)))

        appCompatEditText1.perform(replaceText("lighter.tokyo@gmail.com"), closeSoftKeyboard())

        val appCompatEditText = onView(
                allOf(withId(R.id.fieldPassword),
                        childAtPosition(
                                allOf(withId(R.id.emailPasswordFields),
                                        childAtPosition(
                                                withId(R.id.login_layout),
                                                0)),
                                1),
                        isDisplayed()))
        appCompatEditText.perform(replaceText("moneca01"), closeSoftKeyboard())

        val appCompatEditText2 = onView(
                allOf(withId(R.id.fieldPassword), withText("moneca01"),
                        childAtPosition(
                                allOf(withId(R.id.emailPasswordFields),
                                        childAtPosition(
                                                withId(R.id.login_layout),
                                                0)),
                                1),
                        isDisplayed()))
        appCompatEditText2.perform(pressImeActionButton())

        val appCompatButton2 = onView(
                allOf(withId(R.id.emailSignInButton), withText("ログイン"),
                        childAtPosition(
                                allOf(withId(R.id.emailPasswordButtons),
                                        childAtPosition(
                                                withId(R.id.login_layout),
                                                1)),
                                0),
                        isDisplayed()))
        appCompatButton2.perform(click())

        val bottomNavigationItemView = onView(
                allOf(withId(R.id.navigation_category), withContentDescription("カテゴリ"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0),
                                3),
                        isDisplayed()))
        bottomNavigationItemView.perform(click())

        val floatingActionButton = onView(
                allOf(withId(R.id.add_button),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.frameLayout),
                                        0),
                                2),
                        isDisplayed()))
        floatingActionButton.perform(click())

        val appCompatEditText3 = onView(
                allOf(withId(R.id.category_name),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        0),
                                2),
                        isDisplayed()))
        appCompatEditText3.perform(replaceText("テスト"), closeSoftKeyboard())

        val circleImageView = onView(
                allOf(withId(R.id.color4),
                        childAtPosition(
                                allOf(withId(R.id.grid),
                                        childAtPosition(
                                                withClassName(`is`("android.widget.LinearLayout")),
                                                3)),
                                3),
                        isDisplayed()))
        circleImageView.perform(click())

        val appCompatEditText4 = onView(
                allOf(withId(R.id.category_name), withText("テスト"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        0),
                                2),
                        isDisplayed()))
        appCompatEditText4.perform(replaceText("テスト\n"))

        val appCompatEditText5 = onView(
                allOf(withId(R.id.category_name), withText("テスト\n"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        0),
                                2),
                        isDisplayed()))
        appCompatEditText5.perform(closeSoftKeyboard())

        val appCompatButton3 = onView(
                allOf(withId(R.id.positive_button), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        0),
                                4),
                        isDisplayed()))
        appCompatButton3.perform(click())

        val bottomNavigationItemView2 = onView(
                allOf(withId(R.id.navigation_plan), withContentDescription("計画"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0),
                                0),
                        isDisplayed()))
        bottomNavigationItemView2.perform(click())

        val floatingActionButton2 = onView(
                allOf(withId(R.id.add_button),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.frameLayout),
                                        0),
                                4),
                        isDisplayed()))
        floatingActionButton2.perform(click())

        val appCompatSpinner = onView(
                allOf(withId(R.id.category_spinner),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        0),
                                3),
                        isDisplayed()))
        appCompatSpinner.perform(click())

        val appCompatTextView = onData(anything())
                .inAdapterView(childAtPosition(
                        withClassName(`is`("android.widget.PopupWindow")),
                        0))
                .atPosition(4)
        appCompatTextView.perform(click())

        val appCompatEditText6 = onView(
                allOf(withId(R.id.time_hour), withText("0"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.LinearLayout")),
                                        4),
                                0),
                        isDisplayed()))
        appCompatEditText6.perform(replaceText("10"))

        val appCompatEditText7 = onView(
                allOf(withId(R.id.time_hour), withText("10"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.LinearLayout")),
                                        4),
                                0),
                        isDisplayed()))
        appCompatEditText7.perform(closeSoftKeyboard())

        val appCompatButton4 = onView(
                allOf(withId(R.id.positive_button), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        0),
                                5),
                        isDisplayed()))
        appCompatButton4.perform(click())

        val bottomNavigationItemView3 = onView(
                allOf(withId(R.id.navigation_result), withContentDescription("実績"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0),
                                2),
                        isDisplayed()))
        bottomNavigationItemView3.perform(click())

        val floatingActionButton3 = onView(
                allOf(withId(R.id.add_button),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.LinearLayout")),
                                        1),
                                3),
                        isDisplayed()))
        floatingActionButton3.perform(click())

        val appCompatSpinner2 = onView(
                allOf(withId(R.id.category_spinner),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        0),
                                5),
                        isDisplayed()))
        appCompatSpinner2.perform(click())

        val appCompatTextView2 = onData(anything())
                .inAdapterView(childAtPosition(
                        withClassName(`is`("android.widget.PopupWindow")),
                        0))
                .atPosition(4)
        appCompatTextView2.perform(click())

        val appCompatEditText8 = onView(
                allOf(withId(R.id.time_hour), withText("0"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.LinearLayout")),
                                        6),
                                0),
                        isDisplayed()))
        appCompatEditText8.perform(replaceText("03"))

        val appCompatEditText9 = onView(
                allOf(withId(R.id.time_hour), withText("03"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.LinearLayout")),
                                        6),
                                0),
                        isDisplayed()))
        appCompatEditText9.perform(closeSoftKeyboard())

        val appCompatEditText10 = onView(
                allOf(withId(R.id.time_hour), withText("03"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.LinearLayout")),
                                        6),
                                0),
                        isDisplayed()))
        appCompatEditText10.perform(pressImeActionButton())

        val appCompatButton5 = onView(
                allOf(withId(R.id.positive_button), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        0),
                                7),
                        isDisplayed()))
        appCompatButton5.perform(click())

        val cardView = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.result_recycler),
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        1)),
                        0),
                        isDisplayed()))
        cardView.perform(longClick())

        val cardView2 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.result_recycler),
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        1)),
                        0),
                        isDisplayed()))
        cardView2.perform(click())

        val appCompatButton6 = onView(
                allOf(withId(R.id.close_button), withText("×"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()))
        appCompatButton6.perform(click())

        val appCompatButton7 = onView(
                allOf(withId(android.R.id.button2), withText("Cancel"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                2)))
        appCompatButton7.perform(scrollTo(), click())

        val cardView3 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.result_recycler),
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        1)),
                        0),
                        isDisplayed()))
        cardView3.perform(longClick())

        val cardView4 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.result_recycler),
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        1)),
                        0),
                        isDisplayed()))
        cardView4.perform(click())

        val appCompatButton8 = onView(
                allOf(withId(R.id.close_button), withText("×"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()))
        appCompatButton8.perform(click())

        val appCompatButton9 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                3)))
        appCompatButton9.perform(scrollTo(), click())

        val bottomNavigationItemView4 = onView(
                allOf(withId(R.id.navigation_plan), withContentDescription("計画"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0),
                                0),
                        isDisplayed()))
        bottomNavigationItemView4.perform(click())

        val cardView5 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.plan_recycler),
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        2)),
                        3),
                        isDisplayed()))
        cardView5.perform(longClick())

        val cardView6 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.plan_recycler),
                                childAtPosition(
                                        withClassName(`is`("android.widget.RelativeLayout")),
                                        2)),
                        3),
                        isDisplayed()))
        cardView6.perform(click())

        val appCompatButton10 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                3)))
        appCompatButton10.perform(scrollTo(), click())

        val appCompatButton11 = onView(
                allOf(withId(R.id.close_button), withText("×"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()))
        appCompatButton11.perform(click())

        val bottomNavigationItemView5 = onView(
                allOf(withId(R.id.navigation_setting), withContentDescription("設定"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0),
                                4),
                        isDisplayed()))
        bottomNavigationItemView5.perform(click())

        val linearLayout = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.recycler_view),
                                childAtPosition(
                                        withId(android.R.id.list_container),
                                        0)),
                        2),
                        isDisplayed()))
        linearLayout.perform(click())


        val bottomNavigationItemView6 = onView(
                allOf(withId(R.id.navigation_category), withContentDescription("カテゴリ"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nav_view),
                                        0),
                                3),
                        isDisplayed()))
        bottomNavigationItemView6.perform(click())

        val appCompatButton13 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                3)))
        appCompatButton13.perform(scrollTo(), click())
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
