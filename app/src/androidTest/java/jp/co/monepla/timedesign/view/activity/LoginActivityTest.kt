package jp.co.monepla.timedesign.view.activity


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import jp.co.monepla.timedesign.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class LoginActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(LoginActivity::class.java)

    @Test
    fun loginActivityTest() {
        val appCompatButton = onView(
                allOf(withId(R.id.skip), withText("skip"))).inRoot(isDialog())
        appCompatButton.perform(click())

        val appCompatEditText = onView(
                withId(R.id.fieldEmail)).inRoot(withDecorView(`is`(mActivityTestRule.activity.window.decorView)))
        appCompatEditText.perform(replaceText("lighter.tokyo@gmail.com"), closeSoftKeyboard())

        val appCompatEditText2 = onView(
                allOf(withId(R.id.fieldEmail), withText("lighter.tokyo@gmail.com"),
                        childAtPosition(
                                allOf(withId(R.id.emailPasswordFields),
                                        childAtPosition(
                                                withId(R.id.login_layout),
                                                0)),
                                0),
                        isDisplayed()))
        appCompatEditText2.perform(pressImeActionButton())

        val appCompatEditText3 = onView(
                allOf(withId(R.id.fieldPassword),
                        childAtPosition(
                                allOf(withId(R.id.emailPasswordFields),
                                        childAtPosition(
                                                withId(R.id.login_layout),
                                                0)),
                                1),
                        isDisplayed()))
        appCompatEditText3.perform(replaceText("moneca01"), closeSoftKeyboard())

        val appCompatEditText4 = onView(
                allOf(withId(R.id.fieldPassword), withText("moneca01"),
                        childAtPosition(
                                allOf(withId(R.id.emailPasswordFields),
                                        childAtPosition(
                                                withId(R.id.login_layout),
                                                0)),
                                1),
                        isDisplayed()))
        appCompatEditText4.perform(pressImeActionButton())

        val appCompatButton2 = onView(
                allOf(withId(R.id.emailSignInButton), withText("ログイン"),
                        childAtPosition(
                                allOf(withId(R.id.emailPasswordButtons),
                                        childAtPosition(
                                                withId(R.id.login_layout),
                                                1)),
                                0),
                        isDisplayed()))
        appCompatButton2.perform(click())
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
