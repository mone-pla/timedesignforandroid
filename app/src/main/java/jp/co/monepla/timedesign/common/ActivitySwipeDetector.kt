package jp.co.monepla.timedesign.common

import android.app.Activity
import android.view.MotionEvent
import android.view.View
import kotlin.math.abs

class ActivitySwipeDetector(var activity: Activity,var onSwipeListener:OnSwipeListener) : View.OnTouchListener {
    private val MIN_DISTANCE = 100
    private var downX = 0f
    private var downY: Float = 0f
    private var upX: Float = 0f
    private var upY: Float = 0f
    /**
     * Called when a touch event is dispatched to a view. This allows listeners to
     * get a chance to respond before the target view.
     *
     * @param v The view the touch event has been dispatched to.
     * @param event The MotionEvent object containing full information about
     * the event.
     * @return True if the listener has consumed the event, false otherwise.
     */
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event!!.action) {
            MotionEvent.ACTION_DOWN -> {
                downX = event.x
                downY = event.y
                return true
            }
            MotionEvent.ACTION_UP -> {
                upX = event.x
                upY = event.y
                val deltaX = downX - upX
                // swipe horizontal?
                if (abs(deltaX) > MIN_DISTANCE) { // left or right
                    if (deltaX < 0) {
                        onLeftToRightSwipe()
                        return true
                    }
                    if (deltaX > 0) {
                        onRightToLeftSwipe()
                        return true
                    }
                } else {
                    return false // We don't consume the event
                }
                return true
            }
        }
        return false
    }

    fun onRightToLeftSwipe(){
        onSwipeListener.OnRight()
    }

    fun onLeftToRightSwipe() {
        onSwipeListener.onLeft()
    }
}

interface OnSwipeListener {
    fun onLeft()
    fun OnRight()
}