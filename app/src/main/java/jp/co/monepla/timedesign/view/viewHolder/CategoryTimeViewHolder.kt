package jp.co.monepla.timedesign.view.viewHolder

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.Utils
import jp.co.monepla.timedesign.view.activity.MainActivity

class CategoryTimeViewHolder(itemView: View, private val context: Context) : RecyclerView.ViewHolder(itemView) {
    private val categoryName: AppCompatTextView = itemView.findViewById(R.id.category_name)
    private val time: AppCompatTextView = itemView.findViewById(R.id.time)
    private val layout: RelativeLayout = itemView.findViewById(R.id.category_item)
    private var activity: MainActivity? = null

    fun bind(categoryId : String,t:Int?) {
        val cat
                = getActivity().viewModel.categories.value!!.firstOrNull { category -> category.id == categoryId }
        if(cat != null) {
            categoryName.text = cat.name
            layout.setBackgroundColor(Color.parseColor("#" + cat.color))
        } else {
            categoryName.text = "その他"
        }
        time.text = Utils.getHourMinute(t!!)
    }

    private fun getActivity(): MainActivity {
        if (activity == null) {
            activity = context as MainActivity
        }
        return activity!!
    }
}
