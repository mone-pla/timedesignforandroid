package jp.co.monepla.timedesign.view.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.gms.ads.AdRequest
import com.mixpanel.android.mpmetrics.MixpanelAPI
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.Const
import jp.co.monepla.timedesign.common.OnSaveListener
import jp.co.monepla.timedesign.common.Utils
import jp.co.monepla.timedesign.databinding.ActivityPlanInputBinding
import jp.co.monepla.timedesign.model.Plan
import jp.co.monepla.timedesign.model.Result
import jp.co.monepla.timedesign.repository.PlanRepository
import jp.co.monepla.timedesign.view.adapter.CategorySpinnerAdapter
import jp.co.monepla.timedesign.view.fragment.dialog.TimePickerFragment
import jp.co.monepla.timedesign.viewModel.PlanInputViewModel
import org.joda.time.DateTime
import java.sql.Date
import kotlin.collections.ArrayList

class PlanInputActivity :AppCompatActivity() {
    private var plan = Plan()
    private var planList:List<Plan> = ArrayList()
    private lateinit var datePicker: TimePickerFragment
    private lateinit var viewModel:PlanInputViewModel
    private lateinit var adapter: CategorySpinnerAdapter
    private lateinit var mixPanelAPI: MixpanelAPI
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bind = DataBindingUtil.setContentView<ActivityPlanInputBinding>(this,R.layout.activity_plan_input)
        mixPanelAPI = MixpanelAPI.getInstance(this,Const.MIX_PANEL_TOKEN)
        mixPanelAPI.track("PlanInputActivity")
        viewModel = PlanInputViewModel()
        if(!intent.getBooleanExtra(Const.PARAM_NEW,false)) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            val adRequest = AdRequest.Builder().build()
            bind.adView.loadAd(adRequest)
            bind.adView.visibility = View.VISIBLE
        }
        else bind.secondAction.visibility = View.VISIBLE
        datePicker = TimePickerFragment(object:TimePickerFragment.TimePickerListener {
            override fun onTimeSet(hours: Int, minutes: Int) {
                mixPanelAPI.track("PlanInputActivity",Utils.getJSON("Plan","setTime"))
                plan.time = hours * 60 + minutes
                bind.hour = hours
                bind.minute = minutes
                setRestTime(bind)
            }
        })
        supportActionBar!!.title = getString(R.string.title_dialog_plan)
        plan = if(intent.getSerializableExtra(Const.PARAM_PLAN) == null) Plan() else intent.getSerializableExtra(Const.PARAM_PLAN) as Plan
        bind.hour = plan.time.div(60)
        bind.minute = plan.time % 60

        bind.timeLayout.setOnClickListener {
            mixPanelAPI.track("PlanInputActivity",Utils.getJSON("Plan","timeLayout"))
            datePicker.set(Utils.parseInt(bind.timeHour.text.toString()), Utils.parseInt(bind.timeMinute.text.toString()),false)
            datePicker.show(supportFragmentManager,"fm")
        }
        bind.positiveButton.setOnClickListener { save(bind) }
        setRestTime(bind)
        adapter = CategorySpinnerAdapter(this)
        bind.categorySpinner.adapter = adapter
        viewModel.loadCategory().observe(this, Observer {
            adapter.categories = it
            bind.categorySpinner.adapter = adapter
            adapter.notifyDataSetChanged()
            if (!TextUtils.isEmpty(plan.categoryId)) {
                bind.categorySpinner.setSelection(it.indexOf(it.filter { category ->
                    category.id == plan.categoryId
                }[0]),true)
            } else {
                bind.categorySpinner.setSelection(0,true)
            }
        })

        viewModel.loadPlans().observe(this, Observer {
            planList = it
            setRestTime(bind)
        })
    }

    private fun setRestTime(bind:ActivityPlanInputBinding) {
        val restTime:Int = planList.filter { p -> p.id != plan.id }.map { p -> p.time }.sum()
                + plan.time
        bind.monthTimeHour = restTime.div(60)
        bind.monthTimeMinute = restTime % 60
        val dateTime = DateTime(Date(intent.getLongExtra("month",0)))
        bind.totalTime = Const.DAY_TINE * dateTime.dayOfMonth().withMaximumValue().dayOfMonth / 60
    }

    /**
     * Called when the activity has detected the user's press of the back
     * key. The [OnBackPressedDispatcher][.getOnBackPressedDispatcher] will be given a
     * chance to handle the back button before the default behavior of
     * [android.app.Activity.onBackPressed] is invoked.
     *
     * @see .getOnBackPressedDispatcher
     */
    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     *
     * Derived classes should call through to the base class for it to
     * perform the default menu handling.
     *
     * @param item The menu item that was selected.
     *
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     *
     * @see .onCreateOptionsMenu
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) { finish() }
        return super.onOptionsItemSelected(item)
    }

    private fun save(bind:ActivityPlanInputBinding) {
        mixPanelAPI.track("PlanInputActivity",Utils.getJSON("Plan","save"))
        val hour = if(!bind.timeHour.text.isNullOrBlank()) bind.timeHour.text.toString() else "0"
        val minute = if(!bind.timeMinute.text.isNullOrBlank()) bind.timeMinute.text.toString() else "0"
        plan.time = Integer.parseInt(hour) * 60 + Integer.parseInt(minute)
        if(viewModel.categories.value!!.size > bind.categorySpinner.selectedItemPosition)
        plan.categoryId = viewModel.categories.value!![bind.categorySpinner.selectedItemPosition].id
        PlanRepository().saveItem(plan, object : OnSaveListener {
            override fun onSave(exception: Exception?) {
                if(exception != null) Toast.makeText(this@PlanInputActivity,"",Toast.LENGTH_SHORT).show()
                else if(!intent.getBooleanExtra(Const.PARAM_NEW,false))finish()
                val resultIntent = Intent(this@PlanInputActivity,ResultInputActivity::class.java)
                resultIntent.putExtra(Const.PARAM_NEW,true)
                resultIntent.putExtra(Const.PARAM_RESULT, Result())
                startActivity(resultIntent)
                finish()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        mixPanelAPI.flush()
    }
}