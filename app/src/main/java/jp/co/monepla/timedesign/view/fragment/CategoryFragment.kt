package jp.co.monepla.timedesign.view.fragmimport
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.RequestConfiguration
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.AbstractFragment
import jp.co.monepla.timedesign.common.Const.PARAM_CATEGORY
import jp.co.monepla.timedesign.common.Utils
import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.view.activity.CategoryInputActivity
import jp.co.monepla.timedesign.view.adapter.CategoryAdapter
import jp.co.monepla.timedesign.viewModel.CategoryViewModel


class CategoryFragment : AbstractFragment() {
    private var recyclerView: RecyclerView? = null
    private var categoryViewModel: CategoryViewModel? = null
    private var categoryAdapter: CategoryAdapter? = null

    /**
     * Called when the fragment's activity has been created and this
     * fragment's view hierarchy instantiated.  It can be used to do final
     * initialization once these pieces are in place, such as retrieving
     * views or restoring state.  It is also useful for fragments that use
     * [.setRetainInstance] to retain their instance,
     * as this leftCallback tells the fragment when it is fully associated with
     * the new activity instance.  This is called after [.onCreateView]
     * and before [.onViewStateRestored].
     *
     * @param savedInstanceState If the fragment is being re-created from
     * a previous saved state, this is the state.
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel::class.java)
        categoryViewModel!!.getCategories.observe(this, Observer{ categories ->
            categoryAdapter!!.setData(categories)
            view!!.invalidate()
        })
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * [.onCreate] and [.onActivityCreated].
     *
     *
     * If you return a View from here, you will later be called in
     * [.onDestroyView] when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     * any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     * UI should be attached to.  The fragment should not add the view itself,
     * but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     * from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        Utils.eventLog(context,"Category","Load")
        val view = inflater.inflate(R.layout.fragment_category_list, container, false)
        setLayoutBind(view)
        setLayoutEvent(view)
        setLayoutData()
        return view
    }

    private fun setLayoutBind(view: View) {
        recyclerView = view.findViewById(R.id.category_recycler)
        val adView = view.findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    private fun setLayoutData() {
        categoryAdapter = CategoryAdapter(context!!) {category, b ->
            if(b) {
                Utils.eventLog(context,"Category","Delete")
                // ダイアログを作成して表示
                AlertDialog.Builder(context!!).apply {
                    setTitle(R.string.title_delete)
                    setPositiveButton("OK") { _, _ ->
                        // OKをタップしたときの処理
                        categoryViewModel!!.deleteCategory(category, "")
                        Toast.makeText(context, R.string.deleted, Toast.LENGTH_LONG).show()
                    }
                    setNegativeButton("Cancel", null)
                    show()
                }
            } else {
                Utils.eventLog(context,"Category","Edit")
                val intent = Intent(activity,CategoryInputActivity::class.java)
                intent.putExtra(PARAM_CATEGORY,category)
                startActivity(intent)
            }

        }
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = categoryAdapter
    }

    private fun setLayoutEvent(view: View) {
        view.findViewById<View>(R.id.add_button).setOnClickListener {
            Utils.eventLog(context,"Category","Add")
            val intent = Intent(activity,CategoryInputActivity::class.java)
            intent.putExtra(PARAM_CATEGORY,Category())
            startActivity(intent)
        }

        if(mainActivity == null) return
//        if(mainActivity!!.viewModel.user.value != null && mainActivity!!.viewModel.user.value!!.isPurchaseState) {
//            view.findViewById<AdView>(R.id.adView).visibility = View.GONE
//        }
//
//        mainActivity!!.viewModel.user.observe(this, Observer { user ->
//            if(user != null && user.isPurchaseState) {
//                view.findViewById<AdView>(R.id.adView).visibility = View.GONE
//            }
//        })
    }

}
