package jp.co.monepla.timedesign.common


object Const {
    const val DAY_TINE: Int = 24 * 60
    const val PARAM_NEW = "NEW"
    const val PARAM_CATEGORY = "CATEGORY"
    const val PREF_NOTIFICATION = "Notification"
    const val PREF_REPORT = "REPORT"
    const val PARAM_PLAN ="PLAN"
    const val PARAM_RESULT ="RESULT"
    const val PARAM_PLAN_LIST = "PLAN_LIST"
    const val PARAM_RESULT_LIST = "RESULT_LIST"
    const val RESULT_ACTIVITY_CALENDAR = 101
    const val REQUEST_CODE_PERMISSION = 2
    const val ITEM_ID = "timedesign_premium_plan"
    const val ITEM_STATUS_CODE_INITIAL = 0
    const val MIX_PANEL_TOKEN = "5108e220c28e7fb4c7d8b44fe7e423c2"
    const val PREF_REVIEW = "REVIEW"
}
