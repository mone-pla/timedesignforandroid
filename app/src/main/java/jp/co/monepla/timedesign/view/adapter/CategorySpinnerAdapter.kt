package jp.co.monepla.timedesign.view.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.appcompat.widget.AppCompatTextView
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.model.Category


class CategorySpinnerAdapter(var context:Context) : BaseAdapter() {

    var categories = ArrayList<Category>()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.spinner_item, null)
        }
        view!!.findViewById<AppCompatTextView>(R.id.category_name).text = getItem(position).name
        view.findViewById<AppCompatTextView>(R.id.category_name).setBackgroundColor(getItem(position).getBackground())
        return view
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     * data set.
     * @return The data at the specified position.
     */
    override fun getItem(position: Int): Category {
        return categories[position]
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    override fun getCount(): Int {
        return categories.size
    }


    fun getPosition(id:String) :Int {
        for(i in 0 until count) {
            if(getItem(i).id == id) return i
        }
        return -1
    }
}