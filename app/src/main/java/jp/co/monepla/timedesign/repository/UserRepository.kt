package jp.co.monepla.timedesign.repository

import jp.co.monepla.timedesign.common.FirebaseDatabaseRepository
import jp.co.monepla.timedesign.model.User

class UserRepository :FirebaseDatabaseRepository<User>() {
    override var id: String = ""

    override val rootNode: String
        get() = ""
}