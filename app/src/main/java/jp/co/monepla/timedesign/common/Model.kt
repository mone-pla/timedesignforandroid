package jp.co.monepla.timedesign.common

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import java.io.Serializable

@Suppress("UNCHECKED_CAST")
@IgnoreExtraProperties
open class Model:Serializable {
    @Exclude
    var id: String=""

    fun <T : Model> withId(id: String): T {
        this.id = id
        return this as T
    }
}
