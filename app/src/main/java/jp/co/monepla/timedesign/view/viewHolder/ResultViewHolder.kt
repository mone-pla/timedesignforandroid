package jp.co.monepla.timedesign.view.viewHolder

import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.databinding.CategoryTimeItmBinding
import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.model.Result

class ResultViewHolder(private val binding:CategoryTimeItmBinding) : RecyclerView.ViewHolder(binding.root) {


    fun bind(result: Result,category:Category?) {
        binding.result = result
        binding.category = category
    }


}
