package jp.co.monepla.timedesign.view.fragment

import androidx.fragment.app.Fragment
import android.os.Bundle
import android.provider.ContactsContract.CommonDataKinds.Website.URL
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import jp.co.monepla.timedesign.R


class PageFragment : Fragment() {
    companion object {
        fun newInstance(imageId: Int): PageFragment {
            val fragment = PageFragment()
            val args = Bundle()
            args.putInt("IMAGE", imageId)
            fragment.arguments = args
            return fragment
        }
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null. This will be called between
     * [.onCreate] and [.onActivityCreated].
     *
     * A default View can be returned by calling [.Fragment] in your
     * constructor. Otherwise, this method returns null.
     *
     *
     * It is recommended to **only** inflate the layout in this method and move
     * logic that operates on the returned View to [.onViewCreated].
     *
     *
     * If you return a View from here, you will later be called in
     * [.onDestroyView] when the view is being released.
     *
     * @param inflater The LayoutInflater object that can be used to inflate
     * any views in the fragment,
     * @param container If non-null, this is the parent view that the fragment's
     * UI should be attached to.  The fragment should not add the view itself,
     * but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     * from a previous saved state as given here.
     *
     * @return Return the View for the fragment's UI, or null.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val bundle = arguments
        val view = inflater.inflate(R.layout.fragment_page, container, false)
        val imageView = view.findViewById<AppCompatImageView>(R.id.imageView)
        imageView.setBackgroundResource(bundle!!.getInt("IMAGE"))
        return view
    }
}