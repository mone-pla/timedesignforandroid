package jp.co.monepla.timedesign.repository

import jp.co.monepla.timedesign.common.FirebaseDatabaseRepository
import jp.co.monepla.timedesign.model.Result

class ResultRepository : FirebaseDatabaseRepository<Result>() {
    override var id: String = ""
    override val rootNode: String
        get() = "result"
}
