package jp.co.monepla.timedesign.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.model.Plan
import jp.co.monepla.timedesign.repository.CategoryRepository
import jp.co.monepla.timedesign.repository.PlanRepository
import org.joda.time.DateTime
import java.util.ArrayList

class PlanInputViewModel:ViewModel() {
    var plans: MutableLiveData<ArrayList<Plan>> = MutableLiveData()
    var categories: MutableLiveData<ArrayList<Category>> = MutableLiveData()
    private val planRepository = PlanRepository()
    private val categoryRepository = CategoryRepository()
    val dateTime = MutableLiveData<DateTime>().apply { value = DateTime().withTime(0,0,0,0) }

    fun loadPlans(): LiveData<ArrayList<Plan>> {
        planRepository.saved.whereEqualTo("planMonth",dateTime.value!!.toDate()).addSnapshotListener { queryDocumentSnapshots, e ->
            if (e != null || queryDocumentSnapshots == null) {
                plans = MutableLiveData()
                return@addSnapshotListener
            }
            val planList = ArrayList<Plan>()
            for (doc in queryDocumentSnapshots.documents) {
                val plan = doc.toObject(Plan::class.java)!!.withId<Plan>(doc.id)
                planList.add(plan)
            }
            plans.postValue(planList)
        }
        return plans
    }

    fun loadCategory():LiveData<ArrayList<Category>> {
        categoryRepository.saved.addSnapshotListener { querySnapshot, e ->
            if (e != null || querySnapshot == null) {
                categories = MutableLiveData()
                return@addSnapshotListener
            }
            val categoryList = ArrayList<Category>()
            for (doc in querySnapshot.documents) {
                val category = doc.toObject(Category::class.java)!!.withId<Category>(doc.id)
                categoryList.add(category)
            }
            categories.postValue(categoryList)
        }
        return categories
    }
}