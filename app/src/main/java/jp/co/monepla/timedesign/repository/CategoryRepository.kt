package jp.co.monepla.timedesign.repository

import jp.co.monepla.timedesign.common.FirebaseDatabaseRepository
import jp.co.monepla.timedesign.model.Category

class CategoryRepository : FirebaseDatabaseRepository<Category>() {
    override var id: String = ""
    override val rootNode: String
        get() = "category"
}
