package jp.co.monepla.timedesign.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import java.util.ArrayList

import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.model.Plan
import jp.co.monepla.timedesign.model.Result
import jp.co.monepla.timedesign.repository.CategoryRepository
import jp.co.monepla.timedesign.repository.PlanRepository
import jp.co.monepla.timedesign.repository.ResultRepository

class CategoryViewModel : ViewModel() {
    private var categories: MutableLiveData<List<Category>> = MutableLiveData()
    private val categoryRepository = CategoryRepository()
    private val planRepository = PlanRepository()
    private val resultRepository = ResultRepository()

    val getCategories: LiveData<List<Category>>
        get() {
            categoryRepository.saved.addSnapshotListener { queryDocumentSnapshots, e ->
                if (e != null || queryDocumentSnapshots == null) {
                    categories = MutableLiveData()
                    return@addSnapshotListener
                }
                val resultList = ArrayList<Category>()
                for (doc in queryDocumentSnapshots.documents) {
                    val category = doc.toObject(Category::class.java)!!.withId<Category>(doc.id)
                    resultList.add(category)
                }
                categories.setValue(resultList)
            }
            return categories
        }

    fun deleteCategory(category: Category, otherId: String) {
        categoryRepository.delete(category).addOnFailureListener { e -> println(e.message) }

        planRepository.getNonEventSaved(category.id).get().addOnSuccessListener { queryDocumentSnapshots ->
            for (doc in queryDocumentSnapshots.documents) {
                val plan = doc.toObject(Plan::class.java)!!.withId<Plan>(doc.id)
                plan.categoryId = otherId
                planRepository.updatedItem(plan)
            }
        }

        resultRepository.getNonEventSaved(category.id).get().addOnSuccessListener { queryDocumentSnapshots ->
            for (doc in queryDocumentSnapshots.documents) {
                val result = doc.toObject(Result::class.java)!!.withId<Result>(doc.id)
                result.categoryId = otherId
                resultRepository.updatedItem(result)
            }
        }
    }
}
