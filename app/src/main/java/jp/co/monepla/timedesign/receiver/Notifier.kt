package jp.co.monepla.timedesign.receiver

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.*
import android.database.DatabaseUtils
import android.net.Uri
import android.os.Build
import android.provider.CalendarContract
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.model.Result
import jp.co.monepla.timedesign.repository.CategoryRepository
import jp.co.monepla.timedesign.repository.ResultRepository
import jp.co.monepla.timedesign.view.activity.MainActivity
import org.joda.time.DateTime
import java.util.*


class Notifier :BroadcastReceiver() {
    companion object {
        var NOTIFICATION_ID = "notificationId"
    }
    private val EVENT_PROJECTION_IDX_TITLE = 30
    private val EVENT_PROJECTION_IDX_EVENT_COLOR = 58
    private val EVENT_PROJECTION_IDX_DTSTART = 23
    private val EVENT_PROJECTION_IDX_DTEND = 39
    /**
     * This method is called when the BroadcastReceiver is receiving an Intent
     * broadcast.  During this time you can use the other methods on
     * BroadcastReceiver to view/modify the current result values.  This method
     * is always called within the main thread of its process, unless you
     * explicitly asked for it to be scheduled on a different thread using
     * [android.content.Context.registerReceiver]. When it runs on the main
     * thread you should
     * never perform long-running operations in it (there is a timeout of
     * 10 seconds that the system allows before considering the receiver to
     * be blocked and a candidate to be killed). You cannot launch a popup dialog
     * in your implementation of onReceive().
     *
     *
     * **If this BroadcastReceiver was launched through a &lt;receiver&gt; tag,
     * then the object is no longer alive after returning from this
     * function.** This means you should not perform any operations that
     * return a result to you asynchronously. If you need to perform any follow up
     * background work, schedule a [android.app.job.JobService] with
     * [android.app.job.JobScheduler].
     *
     * If you wish to interact with a service that is already running and previously
     * bound using [bindService()][android.content.Context.bindService],
     * you can use [.peekService].
     *
     *
     * The Intent filters used in [android.content.Context.registerReceiver]
     * and in application manifests are *not* guaranteed to be exclusive. They
     * are hints to the operating system about how to find suitable recipients. It is
     * possible for senders to force delivery to specific recipients, bypassing filter
     * resolution.  For this reason, [onReceive()][.onReceive]
     * implementations should respond only to known actions, ignoring any unexpected
     * Intents that they may receive.
     *
     * @param context The Context in which the receiver is running.
     * @param intent The Intent being received.
     */
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(context: Context, intent: Intent) {
        if(intent.getIntExtra(NOTIFICATION_ID,0) == 1) buildNotification(context,"今日もお疲れ様でした","今日の時間を登録しましょう！")
        if(intent.getIntExtra(NOTIFICATION_ID,0) == 2) autoImportCalendar(context)
    }

    private fun buildNotification(context: Context,title:String,body:String) {
        val resultIntent = Intent(context, MainActivity::class.java)
        val resultPendingIntent: PendingIntent? = TaskStackBuilder.create(context).run {
            // Add the intent, which inflates the back stack
            addNextIntentWithParentStack(resultIntent)
            // Get the PendingIntent containing the entire back stack
            getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            val builder = NotificationCompat.Builder(context, NOTIFICATION_ID)
            builder.setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(R.drawable.icon)
                    .setContentIntent(resultPendingIntent)
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.notify(0,builder.build())
        } else {
            val builder = NotificationCompat.Builder(context, NOTIFICATION_ID)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(R.drawable.icon)
                    .setContentIntent(resultPendingIntent)
            NotificationManagerCompat.from(context).notify(0, builder.build())
        }
    }

    private fun autoImportCalendar(context: Context) {
        val categoryRepository = CategoryRepository()
        categoryRepository.saved.get().addOnCompleteListener { task ->
            if(task.isSuccessful && task.result != null) {
                val categoryList = ArrayList<Category>()
                for (doc in task.result!!.documents){
                    val category = doc.toObject(Category::class.java)!!.withId<Category>(doc.id)
                    categoryList.add(category)
                }
                getCalendar(context,categoryList)
            }
        }

    }

    private fun getCalendar(context: Context,categoryList:ArrayList<Category>) {
        val dateTime = DateTime.now().minusDays(1).withTime(0,0,0,0)
        val resultRepository = ResultRepository()
        // クエリを発行してカーソルを取得する
        val cr: ContentResolver = context.contentResolver
        val eventsUriBuilder: Uri.Builder = CalendarContract.Instances.CONTENT_URI
                .buildUpon()
        ContentUris.appendId(eventsUriBuilder, dateTime.withTime(0, 0, 0, 0).toDate().time)
        ContentUris.appendId(eventsUriBuilder, dateTime.withTime(23, 59, 59, 0).toDate().time)
        val eventsUri: Uri = eventsUriBuilder.build()
        val cur = cr.query(eventsUri, null, null, null, "begin ASC")

        while (cur!!.moveToNext()) {
            Log.v("Cursor Object", DatabaseUtils.dumpCursorToString(cur))
            val dtStart = cur.getLong(EVENT_PROJECTION_IDX_DTSTART)
            val dtEnd = cur.getLong(EVENT_PROJECTION_IDX_DTEND)
            val result = Result(
                    resultDate = dateTime.toDate(),
                    categoryId = categoryList.first { category -> category.color == String.format("%08X", cur.getInt(EVENT_PROJECTION_IDX_EVENT_COLOR)) }.id,
                    title = cur.getString(EVENT_PROJECTION_IDX_TITLE),
                    time = ((dtEnd - dtStart) / 60000).toInt()
            )
            resultRepository.createdItm(result)
        }

        buildNotification(context,"昨日のカレンダーを取り込みました","昨日の時間を振り返りましょう!")
    }

}