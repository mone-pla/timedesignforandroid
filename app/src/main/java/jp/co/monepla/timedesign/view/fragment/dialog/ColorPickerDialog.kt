package jp.co.monepla.timedesign.view.fragment.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.databinding.FragmentDialogColorpickerBinding
import kotlinx.android.synthetic.main.activity_main.*

class ColorPickerDialog(var listener:ColorPickerListener) :DialogFragment() {
    private lateinit var bind: FragmentDialogColorpickerBinding
    interface ColorPickerListener {
        fun onColorSet(color:Int)
    }

    /**
     * Override to build your own custom Dialog container.  This is typically
     * used to show an AlertDialog instead of a generic Dialog; when doing so,
     * [.onCreateView] does not need
     * to be implemented since the AlertDialog takes care of its own content.
     *
     *
     * This method will be called after [.onCreate] and
     * before [.onCreateView].  The
     * default implementation simply instantiates and returns a [Dialog]
     * class.
     *
     *
     * *Note: DialogFragment own the [ Dialog.setOnCancelListener][Dialog.setOnCancelListener] and [ Dialog.setOnDismissListener][Dialog.setOnDismissListener] callbacks.  You must not set them yourself.*
     * To find out about these events, override [.onCancel]
     * and [.onDismiss].
     *
     * @param savedInstanceState The last saved instance state of the Fragment,
     * or null if this is a freshly created Fragment.
     *
     * @return Return a new Dialog instance to be displayed by the Fragment.
     */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        bind = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.fragment_dialog_colorpicker, container, false)

        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        builder.setView(bind.root)
        builder.setPositiveButton(android.R.string.ok) { _, _ ->
            listener.onColorSet(bind.colorPickerView.color)
        }
        builder.setNegativeButton(android.R.string.cancel, null)
        builder.setCancelable(true)
        return builder.create()
    }
}