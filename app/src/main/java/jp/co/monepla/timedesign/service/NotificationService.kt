package jp.co.monepla.timedesign.service


import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import jp.co.monepla.timedesign.R
import org.joda.time.DateTime


class NotificationService: Service() {
    /**
     * Called by the system every time a client explicitly starts the service by calling
     * [android.content.Context.startService], providing the arguments it supplied and a
     * unique integer token representing the start request.  Do not call this method directly.
     *
     *
     * For backwards compatibility, the default implementation calls
     * [.onStart] and returns either [.START_STICKY]
     * or [.START_STICKY_COMPATIBILITY].
     *
     *
     * Note that the system calls this on your
     * service's main thread.  A service's main thread is the same
     * thread where UI operations take place for Activities running in the
     * same process.  You should always avoid stalling the main
     * thread's event loop.  When doing long-running operations,
     * network calls, or heavy disk I/O, you should kick off a new
     * thread, or use [android.os.AsyncTask].
     *
     * @param intent The Intent supplied to [android.content.Context.startService],
     * as given.  This may be null if the service is being restarted after
     * its process has gone away, and it had previously returned anything
     * except [.START_STICKY_COMPATIBILITY].
     * @param flags Additional data about this start request.
     * @param startId A unique integer representing this specific request to
     * start.  Use with [.stopSelfResult].
     *
     * @return The return value indicates what semantics the system should
     * use for the service's current started state.  It may be one of the
     * constants associated with the [.START_CONTINUATION_MASK] bits.
     *
     * @see .stopSelfResult
     */
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
//        val requestCode = intent.getIntExtra("REQUEST_CODE",0)
        val channelId = "register"
//        val pendingIntent = PendingIntent.getActivity(applicationContext,requestCode,intent,PendingIntent.FLAG_UPDATE_CURRENT)
        val manager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channel = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel(channelId,getString(R.string.app_name),NotificationManager.IMPORTANCE_DEFAULT)
        } else {
            TODO("VERSION.SDK_INT < O")
        }
        channel.setSound(null,null)
        channel.enableLights(true)
        channel.lightColor = Color.BLUE
        channel.enableVibration(true)
        manager.createNotificationChannel(channel)
        val notification = NotificationCompat.Builder(applicationContext,channelId)
                .setContentTitle("a")
                .setSmallIcon(R.mipmap.app)
                .build()
        startForeground(1, notification)
        setNextAlarmService()
        return START_NOT_STICKY
    }

    private fun setNextAlarmService() {
        val dateTime = DateTime.now().withTime(22,0,0,0)
        val now = System.currentTimeMillis()
        if(now > dateTime.toDate().time) {
            dateTime.plusDays(1)
        }
        // 15分毎のアラーム設定


        val intent2 = Intent(applicationContext, NotificationService::class.java)

        val startMillis = dateTime.toDate().time

        val pendingIntent = PendingIntent.getService(applicationContext, 0, intent2, 0)
        val alarmManager = applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                startMillis, pendingIntent)
    }

    /**
     * Return the communication channel to the service.  May return null if
     * clients can not bind to the service.  The returned
     * [android.os.IBinder] is usually for a complex interface
     * that has been [described using
 * aidl]({@docRoot}guide/components/aidl.html).
     *
     *
     * *Note that unlike other application components, calls on to the
     * IBinder interface returned here may not happen on the main thread
     * of the process*.  More information about the main thread can be found in
     * [Processes and
 * Threads]({@docRoot}guide/topics/fundamentals/processes-and-threads.html).
     *
     * @param intent The Intent that was used to bind to this service,
     * as given to [ Context.bindService][android.content.Context.bindService].  Note that any extras that were included with
     * the Intent at that point will *not* be seen here.
     *
     * @return Return an IBinder through which clients can call on to the
     * service.
     */
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        stopAlarmService()
        // Service終了
        stopSelf()
    }

    private fun stopAlarmService() {
        val indent = Intent(applicationContext, NotificationService::class.java)
        val pendingIntent = PendingIntent.getService(applicationContext, 0, indent, 0)
        // アラームを解除する
        val alarmManager = applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.cancel(pendingIntent)
    }
}