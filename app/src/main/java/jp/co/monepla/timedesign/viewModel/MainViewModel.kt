package jp.co.monepla.timedesign.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.model.User
import jp.co.monepla.timedesign.repository.CategoryRepository
import jp.co.monepla.timedesign.repository.UserRepository
import java.util.*

class MainViewModel : ViewModel() {
    var categories: MutableLiveData<ArrayList<Category>> = MutableLiveData()
    var user: MutableLiveData<User> = MutableLiveData()
    private val categoryRepository = CategoryRepository()
    private val userRepository = UserRepository()

    fun loadCategories() {
        categoryRepository.saved.addSnapshotListener { queryDocumentSnapshots, e ->
            if (e != null || queryDocumentSnapshots == null) {
                categories = MutableLiveData()
                return@addSnapshotListener
            }
            val categoryList = ArrayList<Category>()

            for (doc in queryDocumentSnapshots.documents) {
                val category = doc.toObject(Category::class.java)!!.withId<Category>(doc.id)
                categoryList.add(category)
            }

            categories.setValue(categoryList)
        }
    }

    fun loadUser() {
        userRepository.userDocument.addSnapshotListener { documentSnapshot, e ->
            if (e != null || documentSnapshot == null) {
                user = MutableLiveData()
                return@addSnapshotListener
            }
            val userData = documentSnapshot.toObject(User::class.java)
            if(userData != null) {
                userData.withId<User>(documentSnapshot.id)
                user.value = userData
            } else {
                user = MutableLiveData()
            }
        }
    }

    fun updateUser(user: User) {
        userRepository.userDocument.set(user).addOnCompleteListener {

        }
    }
}
