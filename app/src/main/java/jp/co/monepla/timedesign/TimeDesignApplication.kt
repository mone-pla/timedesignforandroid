package jp.co.monepla.timedesign

import android.app.Application
import com.google.firebase.analytics.FirebaseAnalytics

class TimeDesignApplication:Application() {
    private var analytic:FirebaseAnalytics? = null
    override fun onCreate() {
        super.onCreate()
        analytic = FirebaseAnalytics.getInstance(this)
    }
    fun getAnalitics() : FirebaseAnalytics? {
        return analytic
    }
}