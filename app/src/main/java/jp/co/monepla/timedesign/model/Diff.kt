package jp.co.monepla.timedesign.model

import java.io.Serializable

class Diff : Serializable {
    var categoryList = ArrayList<Category>()
    var planList: List<Plan> = ArrayList()
    var resultList: List<Result> = ArrayList()
}
