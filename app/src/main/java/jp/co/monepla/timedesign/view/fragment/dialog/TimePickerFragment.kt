package jp.co.monepla.timedesign.view.fragment.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.databinding.FragmentDialogTimepickerBinding
import kotlinx.android.synthetic.main.activity_main.*


class TimePickerFragment(var listener: TimePickerListener) :DialogFragment() {
    var hours = 0
    var minutes = 0
    var isResult = false
    lateinit var bind: FragmentDialogTimepickerBinding

    interface TimePickerListener {
        fun onTimeSet(hours:Int,minutes:Int)
    }

    private fun createPickerView(): View {
        bind = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.fragment_dialog_timepicker, container, false)
        bind.hour.minValue = 0
        bind.minute.minValue = 0
        bind.hour.maxValue = if(isResult) {24}else{744}
        bind.minute.maxValue = 3
        bind.minute.displayedValues = arrayOf("0","15","30","45")
        bind.hour.value = hours
        bind.minute.value = minutes.div(15)
        return bind.root
    }

    fun set(hours: Int,minutes: Int,isResult:Boolean) {
        this.hours = hours
        this.minutes = minutes
        this.isResult = isResult
    }
    /**
     * Override to build your own custom Dialog container.  This is typically
     * used to show an AlertDialog instead of a generic Dialog; when doing so,
     * [.onCreateView] does not need
     * to be implemented since the AlertDialog takes care of its own content.
     *
     *
     * This method will be called after [.onCreate] and
     * before [.onCreateView].  The
     * default implementation simply instantiates and returns a [Dialog]
     * class.
     *
     *
     * *Note: DialogFragment own the [ Dialog.setOnCancelListener][Dialog.setOnCancelListener] and [ Dialog.setOnDismissListener][Dialog.setOnDismissListener] callbacks.  You must not set them yourself.*
     * To find out about these events, override [.onCancel]
     * and [.onDismiss].
     *
     * @param savedInstanceState The last saved instance state of the Fragment,
     * or null if this is a freshly created Fragment.
     *
     * @return Return a new Dialog instance to be displayed by the Fragment.
     */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        builder.setView(createPickerView())
        builder.setPositiveButton(android.R.string.ok) { _, _ ->
            listener.onTimeSet(bind.hour.value, bind.minute.value * 15)
        }
        builder.setNegativeButton(android.R.string.cancel, null)
        builder.setCancelable(true)
        return builder.create()
    }
}