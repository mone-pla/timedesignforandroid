package jp.co.monepla.timedesign.view.fragment.dialog

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.databinding.FragmentDialogCalendarBinding
import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.model.Event
import jp.co.monepla.timedesign.model.Result
import jp.co.monepla.timedesign.repository.CategoryRepository
import jp.co.monepla.timedesign.view.adapter.EventAdapter
import jp.co.monepla.timedesign.viewModel.CalendarViewModel
import org.joda.time.DateTime
import java.util.*
import kotlin.collections.ArrayList


class CalendarActivity: AppCompatActivity() {
    lateinit var bind: FragmentDialogCalendarBinding
    lateinit var viewModel: CalendarViewModel
    lateinit var adapter: EventAdapter
    private var categories = ArrayList<Category>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind = DataBindingUtil.setContentView(this, R.layout.fragment_dialog_calendar)
        bind.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this).get(CalendarViewModel::class.java)
        bind.viewModel.apply { viewModel }
        adapter = EventAdapter {event ->
            val category = categories.firstOrNull { category -> category.color == event.color }
            if(category == null) {
                CategoryRepository().createdItm(Category(name = "カレンダーから",color = event.color)).addOnSuccessListener { documentReference ->
                    dismiss(event,documentReference.id)
                }
            } else {
                dismiss(event,category.id)
            }
        }
        viewModel.getCalendar(this, DateTime())
        val recyclerView = bind.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
        viewModel.categories.observe(this, androidx.lifecycle.Observer {categories ->
            this.categories = categories
            adapter.categories = categories
            adapter.notifyDataSetChanged()
        })
        viewModel.events.observe(this, androidx.lifecycle.Observer { events ->
            adapter.events = events
            adapter.notifyDataSetChanged()
        })

        viewModel.getCategory()
        viewModel.getCalendar(this, DateTime(intent.getLongExtra("date",0L)))
    }

    private fun dismiss(event: Event,categoryId:String) {
        intent.putExtra("result",Result(
                resultDate = Date(intent.getLongExtra("date",0L)),
                categoryId = categoryId,
                time = event.time,
                title = event.title
        ))
        setResult(RESULT_OK,intent)
        finish()
    }
}