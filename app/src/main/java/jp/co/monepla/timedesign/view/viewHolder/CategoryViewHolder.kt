package jp.co.monepla.timedesign.view.viewHolder

import android.annotation.SuppressLint
import android.graphics.Color
import android.text.TextUtils
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView

import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.Utils

class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val categoryName: AppCompatTextView = itemView.findViewById(R.id.category_name)

    @SuppressLint("Range")
    fun bind(category: Category) {
        categoryName.text = category.name
        if(!TextUtils.isEmpty(category.color) && Utils.isColor(category.color)) {
            categoryName.setBackgroundColor(Color.parseColor("#"+category.color))
        }
    }
}
