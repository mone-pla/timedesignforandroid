package jp.co.monepla.timedesign.repository

import jp.co.monepla.timedesign.common.FirebaseDatabaseRepository
import jp.co.monepla.timedesign.model.Plan

class PlanRepository : FirebaseDatabaseRepository<Plan>() {
    override var id: String = ""

    override val rootNode: String
        get() = "planList"
}
