package jp.co.monepla.timedesign.model

import jp.co.monepla.timedesign.common.Model
import org.joda.time.DateTime
import java.io.Serializable
import java.util.Date

data class Plan(
        var planMonth: Date = DateTime().withDayOfMonth(1).withTime(0,0,0,0).toDate(),
        var categoryId: String = "",
        var time: Int = 0
):Model(),Serializable
