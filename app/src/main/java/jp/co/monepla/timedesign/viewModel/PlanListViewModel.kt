package jp.co.monepla.timedesign.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import java.util.ArrayList

import jp.co.monepla.timedesign.model.Plan
import jp.co.monepla.timedesign.repository.PlanRepository

class PlanListViewModel : ViewModel() {

    private var plans: MutableLiveData<ArrayList<Plan>> = MutableLiveData()
    private val planRepository = PlanRepository()


    fun loadPlans(): LiveData<ArrayList<Plan>>? {
        planRepository.saved.addSnapshotListener { queryDocumentSnapshots, e ->
            if (e != null || queryDocumentSnapshots == null) {
                plans = MutableLiveData()
                return@addSnapshotListener
            }
            val planList = ArrayList<Plan>()
            for (doc in queryDocumentSnapshots.documents) {
                val plan = doc.toObject(Plan::class.java)!!.withId<Plan>(doc.id)
                planList.add(plan)
            }
            plans.setValue(planList)
        }
        return plans
    }

    fun deletePlan(plan: Plan) {
        planRepository.delete(plan).addOnFailureListener { e -> println(e.message) }
    }
}
