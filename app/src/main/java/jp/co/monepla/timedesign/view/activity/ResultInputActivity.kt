package jp.co.monepla.timedesign.view.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.ads.AdRequest
import com.mixpanel.android.mpmetrics.MixpanelAPI
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.Const
import jp.co.monepla.timedesign.common.Const.PARAM_RESULT
import jp.co.monepla.timedesign.common.Const.REQUEST_CODE_PERMISSION
import jp.co.monepla.timedesign.common.Const.RESULT_ACTIVITY_CALENDAR
import jp.co.monepla.timedesign.common.OnSaveListener
import jp.co.monepla.timedesign.common.Utils
import jp.co.monepla.timedesign.common.Utils.Companion.parseInt
import jp.co.monepla.timedesign.databinding.ActivityResultInputBinding
import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.model.Result
import jp.co.monepla.timedesign.view.adapter.CategorySpinnerAdapter
import jp.co.monepla.timedesign.view.fragment.dialog.CalendarActivity
import jp.co.monepla.timedesign.view.fragment.dialog.DatePickerFragment
import jp.co.monepla.timedesign.view.fragment.dialog.TimePickerFragment
import jp.co.monepla.timedesign.viewModel.ResultInputViewModel
import org.joda.time.DateTime
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ResultInputActivity :AppCompatActivity() {
    private var result = Result()
    private lateinit var bind: ActivityResultInputBinding
    private lateinit var viewModel: ResultInputViewModel
    private lateinit var timePicker: TimePickerFragment
    private lateinit var datePicker:DatePickerFragment
    private lateinit var adapter: CategorySpinnerAdapter
    private var categoryList = ArrayList<Category>()
    private val simpleDateFormat = SimpleDateFormat("yyyy/MM/dd",Locale.JAPAN)
    private var isNineCount = false
    private lateinit var mixPanelAPI: MixpanelAPI
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind = DataBindingUtil.setContentView(this, R.layout.activity_result_input)
        viewModel = ViewModelProviders.of(this).get(ResultInputViewModel::class.java)
        timePicker = TimePickerFragment(timeSet)
        datePicker = DatePickerFragment(dateSet)
        mixPanelAPI = MixpanelAPI.getInstance(this,Const.MIX_PANEL_TOKEN)
        mixPanelAPI.track("ResultInputActivity")
        if(!intent.getBooleanExtra(Const.PARAM_NEW,false)) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            val adRequest = AdRequest.Builder().build()
            bind.adView.loadAd(adRequest)
            bind.adView.visibility = View.VISIBLE
        }
        else bind.thirdAction.visibility = View.VISIBLE
        supportActionBar!!.title = getString(R.string.title_dialog_result)
        val upArrow = ResourcesCompat.getDrawable(resources,android.R.drawable.ic_menu_close_clear_cancel,null)
        supportActionBar!!.setHomeAsUpIndicator(upArrow)

        categoryList = when {
            "jp.co.monepla.timedesign.action.register" == intent.action -> {
                ArrayList()
            }
            intent.getSerializableExtra(Const.PARAM_CATEGORY) != null -> {
                intent.getSerializableExtra(Const.PARAM_CATEGORY) as ArrayList<Category>
            }
            else -> {
                ArrayList()
            }
        }
        result = when {
            "jp.co.monepla.timedesign.action.register" == intent.action -> {
                Result(resultDate = DateTime().withTime(0,0,0,0).toDate())
            }
            intent.getSerializableExtra(PARAM_RESULT) == null -> {
                Result()
            }
            else -> {
                intent.getSerializableExtra(PARAM_RESULT) as Result
            }
        }
        bind.hour = result.time.div(60)
        bind.minute = result.time % 60
        bind.title = result.title

        adapter = CategorySpinnerAdapter(this)
        adapter.categories = categoryList
        bind.categorySpinner.adapter = adapter
        setCategorySelection()
        bind.timeLayout.setOnClickListener {
            mixPanelAPI.track("ResultInputActivity",Utils.getJSON("Result","timeLayout"))
            timePicker.set(parseInt(bind.timeHour.text.toString()), parseInt(bind.timeMinute.text.toString()), true)
            timePicker.show(supportFragmentManager, "fm")
        }

        if (!TextUtils.isEmpty(result.categoryId)) {
            bind.categorySpinner.setSelection(categoryList.indexOf(categoryList.filter { category ->
                category.id == result.categoryId
            }[0]))
        }
        bind.resultDate = simpleDateFormat.format(result.resultDate)
        bind.calenderView.setOnClickListener {
            mixPanelAPI.track("ResultInputActivity",Utils.getJSON("Result","calendarView"))
            datePicker.date = result.resultDate
            datePicker.show(supportFragmentManager, "fm")
        }
        bind.myTime.isChecked = result.myTime
        bind.priority.isChecked = result.priority
        bind.urgency.isChecked = result.urgency
        bind.positiveButton.setOnClickListener { save(bind, categoryList) }
        viewModel.getResults(DateTime(result.resultDate))
        viewModel.getCategory()
        viewModel.results.observe(this, androidx.lifecycle.Observer { resultList ->
            setRestTime(bind, resultList)
        })
        viewModel.categories.observe(this, androidx.lifecycle.Observer { categoryList ->
            this.categoryList = categoryList
            adapter.categories = categoryList
            adapter.notifyDataSetChanged()
            setCategorySelection()
        })
        viewModel.repository.saved.get().addOnCompleteListener {
            if(it.result != null && it.result!!.count() == 9) {
                isNineCount = true
            }
        }
    }

    /**
     * Called when the activity has detected the user's press of the back
     * key. The [OnBackPressedDispatcher][.getOnBackPressedDispatcher] will be given a
     * chance to handle the back button before the default behavior of
     * [android.app.Activity.onBackPressed] is invoked.
     *
     * @see .getOnBackPressedDispatcher
     */
    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    /**
     * Initialize the contents of the Activity's standard options menu.  You
     * should place your menu items in to <var>menu</var>.
     *
     *
     * This is only called once, the first time the options menu is
     * displayed.  To update the menu every time it is displayed, see
     * [.onPrepareOptionsMenu].
     *
     *
     * The default implementation populates the menu with standard system
     * menu items.  These are placed in the [Menu.CATEGORY_SYSTEM] group so that
     * they will be correctly ordered with application-defined menu items.
     * Deriving classes should always call through to the base implementation.
     *
     *
     * You can safely hold on to <var>menu</var> (and any items created
     * from it), making modifications to it as desired, until the next
     * time onCreateOptionsMenu() is called.
     *
     *
     * When you add items to the menu, you can implement the Activity's
     * [.onOptionsItemSelected] method to handle them there.
     *
     * @param menu The options menu in which you place your items.
     *
     * @return You must return true for the menu to be displayed;
     * if you return false it will not be shown.
     *
     * @see .onPrepareOptionsMenu
     *
     * @see .onOptionsItemSelected
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val ret = super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.calendat_menu, menu)
        return ret
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     *
     * Derived classes should call through to the base class for it to
     * perform the default menu handling.
     *
     * @param item The menu item that was selected.
     *
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     *
     * @see .onCreateOptionsMenu
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        if(item.itemId == R.id.calender) {
            mixPanelAPI.track("ResultInputActivity",Utils.getJSON("Result","getCalendar"))
            val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR)
            if(permissionCheck == PackageManager.PERMISSION_GRANTED) {
                moveCalendarActivity()
            } else {
                // パーミッションのリクエストを表示
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.READ_CALENDAR),REQUEST_CODE_PERMISSION)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun save(bind: ActivityResultInputBinding, categoryList: ArrayList<Category>) {
        mixPanelAPI.track("ResultInputActivity",Utils.getJSON("Result","save"))
        val hour = if (!bind.timeHour.text.isNullOrBlank()) bind.timeHour.text.toString() else "0"
        val minute = if (!bind.timeMinute.text.isNullOrBlank()) bind.timeMinute.text.toString() else "0"
        result.time = Integer.parseInt(hour) * 60 + Integer.parseInt(minute)
        if(categoryList.isNotEmpty()) result.categoryId = categoryList[bind.categorySpinner.selectedItemPosition].id
        result.myTime = bind.myTime.isChecked
        result.priority = bind.priority.isChecked
        result.urgency = bind.urgency.isChecked
        result.title = bind.resultTitle.text.toString()
        viewModel.saveItem(result, object : OnSaveListener {
            override fun onSave(exception: Exception?) {
                if (exception != null) Toast.makeText(this@ResultInputActivity, "", Toast.LENGTH_SHORT).show()
                else if(!intent.getBooleanExtra(Const.PARAM_NEW,false)) {
                    if(isNineCount) {
                        Utils.launchStore(this@ResultInputActivity)
                    }
                    finish()
                }
                val intent = Intent(this@ResultInputActivity,MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        })
    }

    private fun setRestTime(bind: ActivityResultInputBinding, resultList: ArrayList<Result>) {
        result.time = (parseInt(bind.timeHour.text.toString()) * 60 + parseInt(bind.timeMinute.text.toString()))
        val restTime: Int = resultList
                .filter { ret -> ret.resultDate == ret.resultDate && ret.id != this.result.id }
                .map { ret -> ret.time }.sum() + result.time
        bind.dateTimeHour = restTime.div(60)
        bind.dateTimeMinute = restTime % 60
    }

   private val timeSet = object : TimePickerFragment.TimePickerListener {
        override fun onTimeSet(hours: Int, minutes: Int) {
            result.time = hours * 60 + minutes
            bind.hour = hours
            bind.minute = minutes
            setRestTime(bind, viewModel.results.value!!)
        }
    }

    private val dateSet = object : DatePickerFragment.DatePickerListener {
        override fun onDateSet(date: Date) {
            result.resultDate = date
            bind.resultDate = simpleDateFormat.format(result.resultDate)
            viewModel.getResults(DateTime(date))
        }
    }

    /**
     * Dispatch incoming result to the correct fragment.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == RESULT_ACTIVITY_CALENDAR && resultCode == RESULT_OK) {
            result = data!!.getSerializableExtra("result") as Result
            bind.hour = result.time.div(60)
            bind.minute = result.time % 60
            setRestTime(bind, viewModel.results.value!!)
            bind.title = result.title
            setCategorySelection()
        }
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on [.requestPermissions].
     *
     *
     * **Note:** It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     *
     *
     * @param requestCode The request code passed in [.requestPermissions].
     * @param permissions The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     * which is either [android.content.pm.PackageManager.PERMISSION_GRANTED]
     * or [android.content.pm.PackageManager.PERMISSION_DENIED]. Never null.
     *
     * @see .requestPermissions
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_CODE_PERMISSION) {
            if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                moveCalendarActivity()
            }
        }
    }

    private fun setCategorySelection() {
        if(adapter.getPosition(result.categoryId) > 0)
        bind.categorySpinner.setSelection(adapter.getPosition(result.categoryId))
    }

    private fun moveCalendarActivity() {
        val intentCalendar = Intent(this,CalendarActivity::class.java)
        intentCalendar.putExtra("date",result.resultDate.time)
        startActivityForResult(intentCalendar,RESULT_ACTIVITY_CALENDAR)
    }
}