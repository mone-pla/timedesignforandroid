package jp.co.monepla.timedesign.view.activity

import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.Utils

import kotlinx.android.synthetic.main.activity_term.*

class TermActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_term)
        setSupportActionBar(toolbar)
        findViewById<WebView>(R.id.webView).loadUrl("https://note.mu/booomo/n/n5d9c48ff1503")
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        Utils.eventLog(this,"Term","Load")
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setHomeButtonEnabled(true)
        } ?: IllegalAccessException("Toolbar cannot be null")
    }

}
