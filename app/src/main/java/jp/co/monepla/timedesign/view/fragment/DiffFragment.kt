@file:Suppress("KDocUnresolvedReference")

package jp.co.monepla.timedesign.view.fragment

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatSpinner
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.AbstractFragment
import jp.co.monepla.timedesign.common.Const.PREF_REPORT
import jp.co.monepla.timedesign.common.Utils
import jp.co.monepla.timedesign.model.Diff
import jp.co.monepla.timedesign.model.Plan
import jp.co.monepla.timedesign.model.Result
import jp.co.monepla.timedesign.view.adapter.DiffAdapter
import jp.co.monepla.timedesign.viewModel.DiffViewModel
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import kotlin.collections.ArrayList


class DiffFragment : AbstractFragment() {
    private var dateTime: DateTime = DateTime().withTime(0,0,0,0)
    private var diffAdapter: DiffAdapter? = null
    private var diffViewModel: DiffViewModel? = null
    private var recyclerView: RecyclerView? = null
    private var diffMonth: AppCompatTextView? = null
    private var preMonth: AppCompatImageButton? = null
    private var afterMonth: AppCompatImageButton? = null
    private var reportTypeSpinner: AppCompatSpinner? = null
    private var adView:AdView? = null
    private val diff = Diff()
    private var planList = ArrayList<Plan>()
    private var resultList= ArrayList<Result>()

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * [.onCreate] and [.onActivityCreated].
     *
     *
     * If you return a View from here, you will later be called in
     * [.onDestroyView] when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     * any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     * UI should be attached to.  The fragment should not add the view itself,
     * but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     * from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_diff_list, container, false)
        Utils.eventLog(context,"Diff","Load")
        setLayoutBind(view)
        setLayoutEvent()
        setLayoutData()
        return view
    }

    /**
     * Initialize the contents of the Fragment host's standard options menu.  You
     * should place your menu items in to <var>menu</var>.  For this method
     * to be called, you must have first called [.setHasOptionsMenu].  See
     * for more information.
     *
     * @param menu The options menu in which you place your items.
     *
     * @see .setHasOptionsMenu
     *
     * @see .onPrepareOptionsMenu
     *
     * @see .onOptionsItemSelected
     */
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val ret = super.onCreateOptionsMenu(menu,inflater)
        inflater.inflate(R.menu.report_menu, menu)
        val menuItem = menu.findItem(R.id.report_category)
        val spinner = menuItem.actionView as AppCompatSpinner
        val adapter = ArrayAdapter.createFromResource(context!!,
                R.array.report_category, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinner.adapter = adapter
        return ret
    }

    private fun setLayoutData() {
        diffAdapter = DiffAdapter(context!!)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = diffAdapter
        setHeaderData()
    }

    private fun setLayoutBind(view: View) {
        recyclerView = view.findViewById(R.id.diff_recycler)
        diffMonth = view.findViewById(R.id.diff_month)
        preMonth = view.findViewById(R.id.pre_month)
        afterMonth = view.findViewById(R.id.after_month)
        reportTypeSpinner = view.findViewById(R.id.report_type_spinner)
        val spinnerAdapter = ArrayAdapter(context!!,
                R.layout.spinner_item,resources.getStringArray(R.array.report_category))
        reportTypeSpinner!!.adapter = spinnerAdapter
        reportTypeSpinner!!.setSelection(context!!.getSharedPreferences(PREF_REPORT, Context.MODE_PRIVATE).getInt(PREF_REPORT,1))
        adView = view.findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        adView!!.loadAd(adRequest)
    }

    private fun setLayoutEvent() {
        preMonth!!.setOnClickListener {
            addDateTime(-1)
            setAdapterData()
            setHeaderData()
        }

        afterMonth!!.setOnClickListener {
            addDateTime(1)
            setAdapterData()
            setHeaderData()
        }
        reportTypeSpinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            /**
             * Callback method to be invoked when the selection disappears from this
             * view. The selection can disappear for instance when touch is activated
             * or when the adapter becomes empty.
             *
             * @param parent The AdapterView that now contains no selected item.
             */
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            /**
             *
             * Callback method to be invoked when an item in this view has been
             * selected. This callback is invoked only when the newly selected
             * position is different from the previously selected position or if
             * there was no selected item.
             *
             * Implementers can call getItemAtPosition(position) if they need to access the
             * data associated with the selected item.
             *
             * @param parent The AdapterView where the selection happened
             * @param view The view within the AdapterView that was clicked
             * @param position The position of the view in the adapter
             * @param id The row id of the item that is selected
             */
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val share = context!!.getSharedPreferences(PREF_REPORT, Context.MODE_PRIVATE)
                share.edit().putInt(PREF_REPORT,position).apply()
                dateTime = when(position) {
                    0 -> dateTime.dayOfYear().withMinimumValue()
                    1 -> dateTime.dayOfMonth().withMinimumValue()
                    2 -> dateTime.dayOfWeek().withMinimumValue()
                    else -> dateTime
                }
                setHeaderData()
                setAdapterData()
            }


        }
        if(mainActivity == null) return
        diff.categoryList = mainActivity!!.viewModel.categories.value!!
        mainActivity!!.viewModel.categories.observe(viewLifecycleOwner, Observer { categoryList ->
            diff.categoryList = categoryList
        })

        diffViewModel = ViewModelProviders.of(this).get(DiffViewModel::class.java)
        diffViewModel!!.loadPlans().observe(this, Observer{ plans ->
            Utils.eventLog(context,"Diff","Pre")
            planList = plans as ArrayList<Plan>
            setAdapterData()
        })

        diffViewModel!!.getResults().observe(this, Observer{ results ->
            Utils.eventLog(context,"Diff","Next")
            resultList = results as ArrayList<Result>
            setAdapterData()
        })


        if(mainActivity!!.viewModel.user.value != null && mainActivity!!.viewModel.user.value!!.isPurchaseState) {
            adView!!.visibility = View.GONE
        }

        mainActivity!!.viewModel.user.observe(this, Observer { user ->
            if(user != null && user.isPurchaseState) {
                adView!!.visibility = View.GONE
            }
        })
    }

    private fun setAdapterData() {
        val maxDate = maxDate()
        diff.planList = planList
                .filter { plan -> plan.planMonth.time >= dateTime.toDate().time && plan.planMonth.time <= maxDate }
                .toList()
        diff.resultList = resultList
                .filter { result -> result.resultDate.time >= dateTime.toDate().time && result.resultDate.time <= maxDate }
                .toList()
        diffAdapter!!.setData(diff)
    }

    private fun setHeaderData() {
        val format = DateTimeFormat.forPattern("MM-dd")
        val diffMonthStr = when(context!!.getSharedPreferences(PREF_REPORT, Context.MODE_PRIVATE).getInt(PREF_REPORT,1)) {
            0 -> dateTime.year.toString() + "年"
            1 -> dateTime.monthOfYear.toString() + "月"
            2 -> format.print(dateTime) + " ~ " + format.print(dateTime.dayOfWeek().withMaximumValue())
            3 -> format.print(dateTime)
            else -> dateTime.monthOfYear.toString() + "月"
        }
        diffMonth!!.text = diffMonthStr
    }

    private fun addDateTime(num:Int) {
        dateTime = when(context!!.getSharedPreferences(PREF_REPORT, Context.MODE_PRIVATE).getInt(PREF_REPORT,1)) {
            0 -> dateTime.plusYears(num)
            1 -> dateTime.plusMonths(num)
            2 -> dateTime.plusWeeks(num)
            3 -> dateTime.plusDays(num)
            else -> dateTime
        }
    }

    private fun maxDate() :Long {
        return when(context!!.getSharedPreferences(PREF_REPORT, Context.MODE_PRIVATE).getInt(PREF_REPORT,1)) {
            0 -> dateTime.dayOfYear().withMaximumValue().toDate().time
            1 -> dateTime.dayOfMonth().withMaximumValue().toDate().time
            2 -> dateTime.dayOfWeek().withMaximumValue().toDate().time
            3 -> dateTime.toDate().time
            else -> dateTime.dayOfMonth().withMaximumValue().toDate().time
        }
    }
}
