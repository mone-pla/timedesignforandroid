package jp.co.monepla.timedesign.view.viewHolder

import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.Utils
import jp.co.monepla.timedesign.model.Diff

class ReportTwoHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {
    lateinit var oneReport :AppCompatTextView
    lateinit var twoReport :AppCompatTextView
    lateinit var threeReport :AppCompatTextView
    lateinit var fourReport :AppCompatTextView
    lateinit var title: AppCompatTextView
    fun bind(diff: Diff, position:Int) {
        oneReport = itemView.findViewById(R.id.report_one)
        twoReport = itemView.findViewById(R.id.report_two)
        threeReport = itemView.findViewById(R.id.report_three)
        fourReport = itemView.findViewById(R.id.report_four)
        title = itemView.findViewById(R.id.report_title)
        if(position == 2) createPriorityUrgency(diff)
        else createMyTime(diff)
    }

    private fun createPriorityUrgency(diff:Diff) {
        title.text = "重要と緊急"
        val one = diff.resultList.filter { result -> result.priority && result.urgency.not()}.map { result -> result.time }.sum()
        val two = diff.resultList.filter { result -> result.priority && result.urgency}.map { result -> result.time }.sum()
        val three = diff.resultList.filter { result -> result.priority.not() && result.urgency.not()}.map { result -> result.time }.sum()
        val four = diff.resultList.filter { result -> result.priority.not() && result.urgency}.map { result -> result.time }.sum()
        oneReport.text = Utils.getHourMinute(one)
        twoReport.text = Utils.getHourMinute(two)
        threeReport.text = Utils.getHourMinute(three)
        fourReport.text = Utils.getHourMinute(four)
    }

    private fun createMyTime(diff: Diff) {
        title.text = "自分の時間と他人の時間"
        val one = diff.resultList.filter { result -> result.myTime.not() }.map { result -> result.time }.sum()
        val two = diff.resultList.filter { result -> result.myTime }.map { result -> result.time }.sum()
        oneReport.text = Utils.getHourMinute(one)
        twoReport.text = Utils.getHourMinute(two)
        itemView.findViewById<RelativeLayout>(R.id.report_low).visibility = View.GONE
        itemView.findViewById<View>(R.id.report_center).visibility = View.GONE
        itemView.findViewById<AppCompatTextView>(R.id.priority).visibility = View.GONE
        itemView.findViewById<AppCompatTextView>(R.id.urgency).text = "自分の時間"
    }
}