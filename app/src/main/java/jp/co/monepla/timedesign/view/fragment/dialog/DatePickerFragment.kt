package jp.co.monepla.timedesign.view.fragment.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.databinding.FragmentDialogDatepickerBinding
import kotlinx.android.synthetic.main.activity_main.*
import org.joda.time.DateTime
import java.util.*

class DatePickerFragment(var listener:DatePickerListener):DialogFragment(){
    private lateinit var bind: FragmentDialogDatepickerBinding
    var date = Date()
    interface DatePickerListener {
        fun onDateSet(date:Date)
    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        bind = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.fragment_dialog_datepicker, container, false)
        bind.calenderView.date = date.time
        bind.calenderView.setOnDateChangeListener { _, year, month, dayOfMonth ->
            val dateTime = DateTime(year,month+1,dayOfMonth,0,0)
            date = dateTime.toDate()
        }
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        builder.setView(bind.root)
        builder.setPositiveButton(android.R.string.ok) { _, _ ->
            listener.onDateSet(date)
        }
        builder.setNegativeButton(android.R.string.cancel, null)
        builder.setCancelable(true)
        return builder.create()
    }
}