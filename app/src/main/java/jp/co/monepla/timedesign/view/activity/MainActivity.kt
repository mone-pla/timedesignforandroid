package jp.co.monepla.timedesign.view.activity

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.android.billingclient.api.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.mixpanel.android.mpmetrics.MixpanelAPI
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.Const
import jp.co.monepla.timedesign.common.Const.PREF_NOTIFICATION
import jp.co.monepla.timedesign.common.Const.PREF_REVIEW
import jp.co.monepla.timedesign.common.Utils
import jp.co.monepla.timedesign.model.User
import jp.co.monepla.timedesign.view.fragment.DiffFragment
import jp.co.monepla.timedesign.view.fragment.PlanFragment
import jp.co.monepla.timedesign.view.fragment.ResultAdapterFragment
import jp.co.monepla.timedesign.view.fragment.SettingFragment
import jp.co.monepla.timedesign.view.fragmimport.CategoryFragment
import jp.co.monepla.timedesign.viewModel.MainViewModel
import jp.co.recruit_mp.android.rmp_appiraterkit.AppiraterDialogBuilder
import jp.co.recruit_mp.android.rmp_appiraterkit.AppiraterUtils


class MainActivity : AppCompatActivity() {
    lateinit var viewModel: MainViewModel
    lateinit var billingClient: BillingClient
    lateinit var skuDetail: SkuDetails
    lateinit var mixpanelAPI: MixpanelAPI
    var status = -1
    private var fragments = arrayOf(
            PlanFragment(),
            DiffFragment(),
            ResultAdapterFragment(),
            CategoryFragment(),
            SettingFragment()
    )

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener  {item ->
        when (item.itemId) {
            R.id.navigation_plan -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, fragments[0])
                        .commit()
                title = getString(R.string.title_plan)
                mixpanelAPI.track("PlanFragment")
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_diff -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, fragments[1])
                        .commit()
                title = getString(R.string.title_diff)
                mixpanelAPI.track("ReportFragment")
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_result -> {
                val args = Bundle()
                args.putBoolean("register","jp.co.monepla.timedesign.action.register" == intent.action)
                intent.action = ""
                fragments[2].arguments = args
                supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, fragments[2])
                        .commit()
                title = getString(R.string.title_result)
                mixpanelAPI.track("ResultFragment")
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_category -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, fragments[3])
                        .commit()
                title = getString(R.string.title_category)
                mixpanelAPI.track("CategoryFragment")
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_setting -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, fragments[4])
                        .commit()
                title = getString(R.string.title_settings)
                mixpanelAPI.track("SettingFragment")
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Utils.eventLog(this,"Main","Load")
        val navView = findViewById<BottomNavigationView>(R.id.nav_view)
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        mixpanelAPI = MixpanelAPI.getInstance(this, Const.MIX_PANEL_TOKEN)
        title = when {
            "jp.co.monepla.timedesign.action.register" == intent.action -> {
                navView.selectedItemId = R.id.navigation_result
                getString(R.string.title_plan)
            }
            intent.getBooleanExtra("isCategory",false) -> {
                navView.selectedItemId = R.id.navigation_category
                getString(R.string.title_category)
            }
            else -> {
                navView.selectedItemId = R.id.navigation_plan
                getString(R.string.title_plan)
            }
        }


        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.loadCategories()
        viewModel.loadUser()
        billingClient = BillingClient.newBuilder(this).enablePendingPurchases().setListener(purchasesUpdatedListener).build()
        billingClient.startConnection(billingClientStateListener)
        viewModel.user.observe(this, Observer {
            Utils.updateUser(viewModel,status)
        })

        val pref = getSharedPreferences(PREF_NOTIFICATION, Context.MODE_PRIVATE)
        Utils.setAlert(this)
        pref.edit().putInt(PREF_NOTIFICATION,1).apply()

        val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR)
        if(permissionCheck == PackageManager.PERMISSION_GRANTED) return
        // パーミッションのリクエストを表示
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.READ_CALENDAR), Const.REQUEST_CODE_PERMISSION)
    }

    override fun onStart() {
        super.onStart()
        launchStore()
    }

    private var purchasesUpdatedListener: PurchasesUpdatedListener = PurchasesUpdatedListener { p0, p1 ->
        if(p0.responseCode == BillingClient.BillingResponseCode.OK && p1 != null) {
            p1.forEach { p -> handlePurchase(p)}
        }
    }

    private var billingClientStateListener: BillingClientStateListener = object : BillingClientStateListener {
        override fun onBillingServiceDisconnected() {
//            Toast.makeText(this@MainActivity,"接続が破棄されました", Toast.LENGTH_SHORT).show()
        }

        override fun onBillingSetupFinished(p0: BillingResult?) {
            if (p0!!.responseCode  != BillingClient.BillingResponseCode.OK) return
            val purchaseResult = billingClient.queryPurchases(BillingClient.SkuType.SUBS)
            for(purchase in purchaseResult.purchasesList) {
                Utils.updateUser(viewModel,purchase.purchaseState)
                status = purchase.purchaseState
            }

            val skuList = ArrayList<String>()
            skuList.add(Const.ITEM_ID)
            val params = SkuDetailsParams.newBuilder()
            params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS)
            billingClient.querySkuDetailsAsync(params.build()) { billingResult, skuDetailsList ->
                if (billingResult.responseCode != BillingClient.BillingResponseCode.OK || skuDetailsList == null) return@querySkuDetailsAsync
                for (skuDetails in skuDetailsList) {
                    if (Const.ITEM_ID == skuDetails.sku) {
                        skuDetail = skuDetails
                    }
                }
            }
            if(status != Purchase.PurchaseState.PURCHASED) {
                Utils.removeCalendar(this@MainActivity)
            }
        }
    }

    private fun handlePurchase(purchase: Purchase) {
        when (purchase.purchaseState) {
            Purchase.PurchaseState.PURCHASED -> {
                Utils.setAutoImportCalendar(this)
                var user = viewModel.user.value
                if(user == null) {
                    user = User(true)
                }
                user.isPurchaseState = true
                viewModel.updateUser(user)
                Utils.updateUser(viewModel,purchase.purchaseState)
                status = purchase.purchaseState

            }
            Purchase.PurchaseState.PENDING -> {
                Toast.makeText(this,"保留しました",Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(this,"中止しました",Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mixpanelAPI.flush()
    }

    private fun launchStore() {
        val share = getSharedPreferences(PREF_REVIEW, MODE_PRIVATE)
        if(share.getInt(PREF_REVIEW,0) == 5) {
            // レビューダイアログを生成する
            val builder = AppiraterDialogBuilder(this)
            builder
                    // ダイアログのタイトル
                    .setTitle(R.string.reply_title)
                    // ストアのアプリページに遷移するボタン
                    .addButton(R.string.review_five, fiveClick)
                    .addButton(R.string.review_later) {
                        share.edit().putInt(PREF_REVIEW,0).apply()
                        it.dismiss()
                    }.addButton(R.string.review_cancel) {
                        share.edit().putInt(PREF_REVIEW,6).apply()
                        it.dismiss()
                    }
            val dialog = builder.create()
            dialog.show()
        } else {
            share.edit().putInt(PREF_REVIEW,share.getInt(PREF_REVIEW,0)+1).apply()
        }
    }

    private var fiveClick :AppiraterDialogBuilder.OnClickListener = AppiraterDialogBuilder.OnClickListener { dialog ->
        AppiraterUtils.launchStore(this)
        dialog!!.dismiss()
        getSharedPreferences(PREF_REVIEW, MODE_PRIVATE).edit()
                .putInt(PREF_REVIEW,getSharedPreferences(PREF_REVIEW, MODE_PRIVATE)
                        .getInt(PREF_REVIEW,0)+1).apply()
    }

}
