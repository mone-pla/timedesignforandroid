package jp.co.monepla.timedesign.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.ListenerRegistration
import jp.co.monepla.timedesign.common.Utils
import jp.co.monepla.timedesign.model.Result
import jp.co.monepla.timedesign.repository.ResultRepository
import org.joda.time.DateTime

class ResultViewModel : ViewModel() {
    var results: MutableLiveData<ArrayList<Result>> = MutableLiveData()
    private val resultRepository = ResultRepository()
    val dateTime = MutableLiveData<DateTime>().apply { value = DateTime().withTime(0,0,0,0) }
    var time= MutableLiveData<String>().apply { value = "0" }
    var snapshot:ListenerRegistration? = null

    fun getResults() {
        snapshot = resultRepository.saved.whereEqualTo("resultDate",dateTime.value!!.toDate())
                .addSnapshotListener { queryDocumentSnapshots, e ->
            val resultList = ArrayList<Result>()
            if (e != null || queryDocumentSnapshots == null) {
                return@addSnapshotListener
            }

            for (doc in queryDocumentSnapshots.documents) {
                val result = doc.toObject(Result::class.java)!!.withId<Result>(doc.id)
                resultList.add(result)
            }
            results.value = resultList.sortedBy { result -> result.categoryId }.toCollection(arrayListOf())
            time.apply { value = Utils.getHourMinute(resultList.map { result -> result.time }.sum()) }
        }
    }

    fun deleteResult(result: Result) {
        resultRepository.delete(result).addOnFailureListener { e -> println(e.message) }
    }
}
