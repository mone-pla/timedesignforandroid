package jp.co.monepla.timedesign.view.viewHolder

import androidx.recyclerview.widget.RecyclerView
import jp.co.monepla.timedesign.databinding.EventItmBinding
import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.model.Event

class EventViewHolder(var binding: EventItmBinding) :RecyclerView.ViewHolder(binding.root) {
    fun bind(event: Event,categories :ArrayList<Category>) {
        binding.event = event
        val category = categories.firstOrNull { category -> category.color == event.color }
        if(category != null) {binding.category = category.name}
    }
}