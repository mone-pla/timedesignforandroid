package jp.co.monepla.timedesign.model

import android.graphics.Color
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

data class Event(
        var title:String = "",
        var startDate:Date = Date(),
        var endDate:Date = Date(),
        var time:Int = 0,
        var color:String = ""

):Serializable {
    private val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.JAPAN)
    fun startDateString() : String {
        return dateFormat.format(startDate)
    }

    fun endDateString() : String {
        return dateFormat.format(endDate)
    }

    fun timeString() :String {
        return time.div(60).toString() + " : " + (time%60).toString() + " h"
    }

    fun getBackground() : Int {
        return Color.parseColor("#$color")
    }
}