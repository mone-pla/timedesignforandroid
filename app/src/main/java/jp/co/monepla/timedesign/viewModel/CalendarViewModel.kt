package jp.co.monepla.timedesign.viewModel

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentResolver
import android.content.ContentUris
import android.net.Uri
import android.provider.CalendarContract
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.model.Event
import jp.co.monepla.timedesign.repository.CategoryRepository
import org.joda.time.DateTime
import java.util.*

class CalendarViewModel:ViewModel() {
    var events : MutableLiveData<ArrayList<Event>> = MutableLiveData()
    var categories: MutableLiveData<ArrayList<Category>> = MutableLiveData()
    private val categoryRepository = CategoryRepository()

    // プロジェクション配列のインデックス。
    private val EVENT_PROJECTION_IDX_TITLE = 30
    private val EVENT_PROJECTION_IDX_EVENT_COLOR = 58
    private val EVENT_PROJECTION_IDX_DTSTART = 23
    private val EVENT_PROJECTION_IDX_DTEND = 39

    fun getCategory() {
        categoryRepository.saved.addSnapshotListener { querySnapshot, e ->
            if (e != null || querySnapshot == null) {
                categories = MutableLiveData()
                return@addSnapshotListener
            }
            val categoryList = ArrayList<Category>()
            for (doc in querySnapshot.documents) {
                val category = doc.toObject(Category::class.java)!!.withId<Category>(doc.id)
                categoryList.add(category)
            }
            categories.setValue(categoryList)
        }
    }

    @SuppressLint("Recycle")
    fun getCalendar(activity:Activity?,dateTime: DateTime) {

        // クエリを発行してカーソルを取得する
        val cr: ContentResolver = activity!!.contentResolver
        val eventsUriBuilder: Uri.Builder = CalendarContract.Instances.CONTENT_URI
                .buildUpon()
        ContentUris.appendId(eventsUriBuilder, dateTime.withTime(0,0,0,0).toDate().time)
        ContentUris.appendId(eventsUriBuilder, dateTime.withTime(23,59,59,0).toDate().time)
        val eventsUri: Uri = eventsUriBuilder.build()
        val cur = cr.query(eventsUri, null, null, null, "begin ASC")
        val eventList = ArrayList<Event>()
        while (cur!!.moveToNext()) {
            val dtStart = cur.getLong(EVENT_PROJECTION_IDX_DTSTART)
            val dtEnd = cur.getLong(EVENT_PROJECTION_IDX_DTEND)
            if(cur.getString(EVENT_PROJECTION_IDX_TITLE) == null) continue
            val event = Event(
                    title = cur.getString(EVENT_PROJECTION_IDX_TITLE),
                    startDate = Date(dtStart),
                    endDate = Date(dtEnd),
                    time = ((dtEnd - dtStart) / 60000).toInt(),
                    color = String.format("%08X", cur.getInt(EVENT_PROJECTION_IDX_EVENT_COLOR))
            )
            eventList.add(event)
        }
        events.value = eventList
    }
}