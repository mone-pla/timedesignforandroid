package jp.co.monepla.timedesign.model

import jp.co.monepla.timedesign.common.Model

data class User(
        var isPurchaseState:Boolean = false
):Model()