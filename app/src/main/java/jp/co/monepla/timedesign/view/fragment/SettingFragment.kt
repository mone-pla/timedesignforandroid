package jp.co.monepla.timedesign.view.fragment

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.core.app.ShareCompat
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceScreen
import com.android.billingclient.api.*
import com.google.firebase.auth.FirebaseAuth.getInstance
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.Utils
import jp.co.monepla.timedesign.view.activity.LoginActivity
import jp.co.monepla.timedesign.view.activity.MainActivity
import jp.co.recruit_mp.android.rmp_appiraterkit.AppiraterDialogBuilder
import jp.co.recruit_mp.android.rmp_appiraterkit.AppiraterUtils


class SettingFragment : PreferenceFragmentCompat() {

    /**
     * Called during [.onCreate] to supply the preferences for this fragment.
     * Subclasses are expected to call [.setPreferenceScreen] either
     * directly or via helper methods such as [.addPreferencesFromResource].
     *
     * @param savedInstanceState If the fragment is being re-created from a previous saved state,
     * this is the state.
     * @param rootKey            If non-null, this preference fragment should be rooted at the
     * [PreferenceScreen] with this key.
     */
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.root_preferences)

        findPreference<Preference>("password")!!.setOnPreferenceClickListener {
            Utils.eventLog(context,"Setting","PasswordChangeoad")
            getInstance().sendPasswordResetEmail(getInstance().currentUser!!.email!!).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(context,R.string.send_mail_change_password,Toast.LENGTH_LONG).show()
                }
            }
            false
        }
        findPreference<Preference>("logout")!!.title = if(getInstance().currentUser!!.isAnonymous) {"登録・ログインする"} else {getString(R.string.logout)}
        findPreference<Preference>("logout")!!.setOnPreferenceClickListener {
            Utils.eventLog(context,"Setting","Logout")
            val intent = Intent(this.context, LoginActivity::class.java)
            getInstance().signOut()
            startActivity(intent)
            activity!!.finish()
            false
        }
        findPreference<Preference>("privacy")!!.setOnPreferenceClickListener {
            Utils.eventLog(context,"Setting","Term")
            val uri = Uri.parse("https://note.mu/booomo/n/n5d9c48ff1503")
            val intent = Intent(Intent.ACTION_VIEW,uri)
            startActivity(intent)
            false
        }
        findPreference<Preference>("contact")!!.setOnPreferenceClickListener {
            Utils.eventLog(context,"Setting","Contact")
            val uri = Uri.parse("https://forms.gle/DpfhaD6sTzseRL4B7")
            val intent = Intent(Intent.ACTION_VIEW,uri)
            startActivity(intent)
            false
        }
        findPreference<Preference>("share")!!.setOnPreferenceClickListener {
            Utils.eventLog(context,"Setting","Share")
            share()
            false
        }
        findPreference<Preference>("review")!!.setOnPreferenceClickListener {
            Utils.eventLog(context,"Setting","Review")
            Utils.launchStore(context!!)
            false
        }
        findPreference<Preference>("notification")!!.setOnPreferenceClickListener {
            Utils.eventLog(context,"Setting","notification")
            if(context!=null){
                val intent = Intent()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    intent.action = Settings.ACTION_APP_NOTIFICATION_SETTINGS
                    intent.putExtra(Settings.EXTRA_APP_PACKAGE, context!!.packageName)
                } else {
                    intent.action = "android.settings.APP_NOTIFICATION_SETTINGS"
                    intent.putExtra("app_package", context!!.packageName)
                    intent.putExtra("app_uid", context!!.applicationInfo.uid)
                }
                startActivity(intent)
            }
            false
        }
//        findPreference<Preference>("calendar")!!.setOnPreferenceClickListener {
//            Utils.eventLog(context,"Setting","calendar")
////            Utils.setAutoImportCalendar(context = context!!)
//            buy()
//            false
//        }
    }

    private fun share() {
        val builder = ShareCompat.IntentBuilder.from(activity!!)
        builder.setChooserTitle("アプリを選択")
        builder.setSubject("Time Design(時間簿)")
        builder.setText("#timedesign\\nhttps://play.google.com/store/apps/details?id=jp.co.monepla.timedesign")
        builder.setType("text/plain")
        builder.startChooser()
    }



    private fun buy() {
        val mainActivity = activity as MainActivity
        val flowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(mainActivity.skuDetail)
                .build()
        mainActivity.billingClient.launchBillingFlow(mainActivity, flowParams)
    }
}
