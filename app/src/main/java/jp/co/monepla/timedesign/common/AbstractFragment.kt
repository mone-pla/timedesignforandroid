package jp.co.monepla.timedesign.common

import androidx.fragment.app.Fragment
import jp.co.monepla.timedesign.TimeDesignApplication

import jp.co.monepla.timedesign.view.activity.MainActivity

abstract class AbstractFragment : Fragment() {
    private var activity: MainActivity? = null
    private var app: TimeDesignApplication? = null

    protected val mainActivity: MainActivity?
        get() {
            if (activity == null) {
                activity = getActivity() as MainActivity?
            }
            return activity
        }
    protected val application: TimeDesignApplication ?
        get() {
            if(app == null) {
                app = context as TimeDesignApplication
            }
            return app
        }
}
