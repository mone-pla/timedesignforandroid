package jp.co.monepla.timedesign.view.activity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.gms.tasks.Tasks
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.Const
import jp.co.monepla.timedesign.common.Utils
import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.repository.CategoryRepository
import jp.co.monepla.timedesign.repository.PlanRepository
import jp.co.monepla.timedesign.repository.ResultRepository
import java.util.regex.Matcher
import java.util.regex.Pattern


class LoginActivity : AppCompatActivity() {
    private var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private var mEmailField: TextInputEditText? = null
    private var mPasswordField: TextInputEditText? = null
    var isValidEmail = false
    var isValidPassword = false
    @VisibleForTesting
    var mProgressDialog: ProgressBar? = null

    private val createAccount = {
        showProgressDialog()
        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(mEmailField!!.text!!.toString(), mPasswordField!!.text!!.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Utils.eventLog(this,"Login","SignUp:Ok")
                        hideProgressDialog()
                        updateUI(mAuth.currentUser,true)
                    } else {
                        Utils.eventLog(this,"Login","SignUp:Fail")
                        // If sign in fails, display a message to the user.
                        Toast.makeText(this@LoginActivity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        updateUI(null,false)
                        // [START_EXCLUDE]
                        hideProgressDialog()
                        // [END_EXCLUDE]
                    }
                }
        // [END create_user_with_email]
    }

    private val signIn = {
        showProgressDialog()
        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(mEmailField!!.text!!.toString(), mPasswordField!!.text!!.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Utils.eventLog(this,"Login","SignIn:OK")
                        // Sign in success, update UI with the signed-in user's information
                        val user = mAuth.currentUser
                        updateUI(user,false)
                    } else {
                        Utils.eventLog(this,"Login","SignIn:Fail")
                        // If sign in fails, display a message to the user.
                        Toast.makeText(this@LoginActivity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        updateUI(null,false)
                    }
                    hideProgressDialog()
                    // [END_EXCLUDE]
                }

        // [END sign_in_with_email]
    }

    private fun nonLogin() {
        showProgressDialog()
        mAuth.signInAnonymously().addOnCompleteListener {task ->
            if (task.isSuccessful) {
                Utils.eventLog(this,"Login","SignIn:OK")
                // Sign in success, update UI with the signed-in user's information
                val user = mAuth.currentUser
                updateUI(user,true)
            } else {
                Utils.eventLog(this,"Login","SignIn:Fail")
                // If sign in fails, display a message to the user.
                Toast.makeText(this@LoginActivity, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                updateUI(null,false)
            }
            hideProgressDialog()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firebase_ui)
        Utils.eventLog(this,"Login","Load")
        mEmailField = findViewById(R.id.fieldEmail)
        mPasswordField = findViewById(R.id.fieldPassword)
        mEmailField!!.addTextChangedListener(GenericTextWatcher(R.id.fieldEmail,this))
        mPasswordField!!.addTextChangedListener(GenericTextWatcher(R.id.fieldPassword,this))
        mProgressDialog = findViewById(R.id.progress)
        findViewById<View>(R.id.emailSignInButton).setOnClickListener {
            Utils.eventLog(this,"Login","SignIn")
            if(validateForm()) signIn()
        }
        findViewById<View>(R.id.emailCreateAccountButton).setOnClickListener {
            Utils.eventLog(this,"Login","SignUp")
            if(validateForm()) createAccount()
        }
        findViewById<View>(R.id.anonymousCreateAccountButton).setOnClickListener {
            nonLogin()
        }

        findViewById<AppCompatTextView>(R.id.privacy).setOnClickListener {
            Utils.eventLog(this,"Login","Move:Term")
            val intent = Intent(this,TermActivity::class.java)
            startActivity(intent)
        }

    }

    private fun showProgressDialog() {
        mProgressDialog = ProgressBar(this)
        mProgressDialog!!.isIndeterminate = true
        mProgressDialog!!.visibility = ProgressBar.VISIBLE
    }

    private fun hideProgressDialog() {
        mProgressDialog!!.visibility = ProgressBar.GONE
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = mAuth.currentUser
        updateUI(currentUser,false)
        if(currentUser == null) {
//            val dialogImages = ImageDialogFragment()
//            dialogImages.show(supportFragmentManager,"チュートリアル")
            findViewById<RelativeLayout>(R.id.login_layout).visibility = View.VISIBLE
        } else {
            Utils.eventLog(this,"Login","AutoLogin")
        }
    }

    public override fun onStop() {
        super.onStop()
        hideProgressDialog()
    }

    private fun validateForm(): Boolean {
        var valid = true

        val email = mEmailField!!.text!!.toString()
        if (TextUtils.isEmpty(email)) {
            mEmailField!!.error = "Required."
            valid = false
        } else if (!Utils.isEmailValid(email)) {
            mEmailField!!.error = "Required."
            valid = false
        } else {
            mEmailField!!.error = null
        }

        val password = mPasswordField!!.text!!.toString()
        if (TextUtils.isEmpty(password)) {
            mPasswordField!!.error = "Required."
            valid = false
        } else {
            mPasswordField!!.error = null
        }

        return valid
    }

    private fun updateUI(user: FirebaseUser?, isCategory:Boolean) {
        hideProgressDialog()
        if (user != null && FirebaseAuth.getInstance().currentUser != null) {
            Utils.eventLog(this,"Login","Move:Main")

            if(isCategory) {
                val intent = Intent(this, CategoryInputActivity::class.java)
                intent.putExtra(Const.PARAM_NEW,isCategory)
                intent.putExtra(Const.PARAM_CATEGORY,Category())
                startActivity(intent)
                finish()
            }
            getMoveActivity()
        } else {
            findViewById<RelativeLayout>(R.id.login_layout).visibility = View.VISIBLE
        }
    }



    private class GenericTextWatcher(var editTextId:Int,var activity: LoginActivity): TextWatcher {

        /**
         * This method is called to notify you that, somewhere within
         * `s`, the text has been changed.
         * It is legitimate to make further changes to `s` from
         * this callback, but be careful not to get yourself into an infinite
         * loop, because any changes you make will cause this method to be
         * called again recursively.
         * (You are not told where the change took place because other
         * afterTextChanged() methods may already have made other changes
         * and invalidated the offsets.  But if you need to know here,
         * to mark your place and then look up from here where the span
         * ended up.
         */
        override fun afterTextChanged(s: Editable?) {
            if (editTextId == R.id.fieldEmail) validationEmail(s)
            if (editTextId == R.id.fieldPassword) validationPassword(s)
        }

        /**
         * This method is called to notify you that, within `s`,
         * the `count` characters beginning at `start`
         * are about to be replaced by new text with length `after`.
         * It is an error to attempt to make changes to `s` from
         * this callback.
         */
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        /**
         * This method is called to notify you that, within `s`,
         * the `count` characters beginning at `start`
         * have just replaced old text that had length `before`.
         * It is an error to attempt to make changes to `s` from
         * this callback.
         */
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }

        private fun validationEmail(s: Editable?) {
            activity.isValidEmail = s.toString().length <= 255
            if (!activity.isValidEmail) {
                val errMsg: String = java.lang.String.format(activity.getString(R.string.input_too_long),0, 255)
                activity.mEmailField?.error = errMsg
                activity.findViewById<Button>(R.id.emailSignInButton).isEnabled = false
                activity.findViewById<Button>(R.id.emailCreateAccountButton).isEnabled = false
                return
            }

            val regex = "\\A[\\w+\\-.]+@[a-z\\d\\-.]+\\.[a-z]+\\z"
            val p: Pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE)
            val m: Matcher = p.matcher(s.toString())
            activity.isValidEmail = m.find()

            if (!activity.isValidEmail) {
                val errMsg: String = activity.getString(R.string.signup_email)
                activity.mEmailField?.error = errMsg
                activity.findViewById<Button>(R.id.emailSignInButton).isEnabled = false
                activity.findViewById<Button>(R.id.emailCreateAccountButton).isEnabled = false

                return
            }

            activity.isValidEmail = true
            if (activity.isValidEmail && activity.isValidPassword) {
                activity.findViewById<Button>(R.id.emailSignInButton).isEnabled = true
                activity.findViewById<Button>(R.id.emailCreateAccountButton).isEnabled = true
            }
        }

        private fun validationPassword(s:Editable?) {
            activity.isValidPassword = s.toString().length in 4..255
            if (!activity.isValidPassword) {
            val errMsg: String = java.lang.String.format(activity.getString(R.string.input_too_long),4, 255)
                activity.mPasswordField?.error = errMsg
                activity.findViewById<Button>(R.id.emailSignInButton).isEnabled = false
                activity.findViewById<Button>(R.id.emailCreateAccountButton).isEnabled = false
                return
            }
            activity.isValidPassword = true
            if (activity.isValidEmail && activity.isValidPassword) {
                activity.findViewById<Button>(R.id.emailSignInButton).isEnabled = true
                activity.findViewById<Button>(R.id.emailCreateAccountButton).isEnabled = true
            }
        }
    }

    private fun getMoveActivity() {
        findViewById<RelativeLayout>(R.id.iconLayout).layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        val categoryTask = CategoryRepository().saved.limit(1).get()
        val planTask = PlanRepository().saved.limit(1).get()
        val resultTask = ResultRepository().saved.limit(1).get()
        Tasks.whenAllComplete(categoryTask,planTask,resultTask)
                .addOnCompleteListener {
                    if(categoryTask.result == null || categoryTask.result!!.count() == 0) {
                        move(CategoryInputActivity::class.java)
                    } else if(planTask.result == null || planTask.result!!.count() == 0) {
                        move(PlanInputActivity::class.java)
                    } else if(resultTask.result == null || resultTask.result!!.count() == 0) {
                        move(ResultInputActivity::class.java)
                    } else {
                        move(MainActivity::class.java)
                    }

        }
    }

    private fun <T:AppCompatActivity>move(activity: Class<T>) {
        val intent = Intent(this, activity)
        intent.putExtra(Const.PARAM_NEW,true)
        startActivity(intent)
        finish()
    }
}
