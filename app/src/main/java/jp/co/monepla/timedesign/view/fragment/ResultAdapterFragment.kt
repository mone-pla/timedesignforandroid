package jp.co.monepla.timedesign.view.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import devs.mulham.horizontalcalendar.HorizontalCalendar
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.AbstractFragment
import jp.co.monepla.timedesign.common.ActivitySwipeDetector
import jp.co.monepla.timedesign.common.Const.PARAM_CATEGORY
import jp.co.monepla.timedesign.common.Const.PARAM_RESULT
import jp.co.monepla.timedesign.common.Const.PARAM_RESULT_LIST
import jp.co.monepla.timedesign.common.OnSwipeListener
import jp.co.monepla.timedesign.common.Utils
import jp.co.monepla.timedesign.databinding.FragmentResultAdapterBinding
import jp.co.monepla.timedesign.model.Result
import jp.co.monepla.timedesign.view.activity.ResultInputActivity
import jp.co.monepla.timedesign.view.adapter.ResultAdapter
import jp.co.monepla.timedesign.viewModel.ResultViewModel
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.util.*


class ResultAdapterFragment :AbstractFragment() {
    var dateTime:DateTime = DateTime().withTime(0,0,0,0)
    private lateinit var adapter: ResultAdapter
    private lateinit var viewModel: ResultViewModel
    private lateinit var weekCalendar: HorizontalCalendar
    private var DEF_FMT: DateTimeFormatter = DateTimeFormat.forPattern("MM YYYY")

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null. This will be called between
     * [.onCreate] and [.onActivityCreated].
     *
     * A default View can be returned by calling [.Fragment] in your
     * constructor. Otherwise, this method returns null.
     *
     *
     * It is recommended to **only** inflate the layout in this method and move
     * logic that operates on the returned View to [.onViewCreated].
     *
     *
     * If you return a View from here, you will later be called in
     * [.onDestroyView] when the view is being released.
     *
     * @param inflater The LayoutInflater object that can be used to inflate
     * any views in the fragment,
     * @param container If non-null, this is the parent view that the fragment's
     * UI should be attached to.  The fragment should not add the view itself,
     * but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     * from a previous saved state as given here.
     *
     * @return Return the View for the fragment's UI, or null.
     */
    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val bind = DataBindingUtil.inflate<FragmentResultAdapterBinding>(inflater, R.layout.fragment_result_adapter, container, false)
        viewModel = ViewModelProviders.of(this).get(ResultViewModel::class.java)
        bind.lifecycleOwner = this
        bind.isCalendar = false
        val startCal = dateTime.toCalendar(Locale.JAPAN)
        val endCal = dateTime.toCalendar(Locale.JAPAN)
        startCal.add(Calendar.YEAR,-100)
        endCal.add(Calendar.YEAR,1)
        bind.time = "0"
        weekCalendar = HorizontalCalendar.Builder(bind.root, R.id.result_calendar)
                .range(startCal,endCal)
                .defaultSelectedDate(dateTime.toCalendar(Locale.JAPAN))
                .build()
        val recyclerView = bind.resultRecycler
        val calendarView = bind.calenderView

        calendarView.date = dateTime.toDate().time
        adapter = if(mainActivity == null || mainActivity!!.viewModel.categories.value == null) {
            ResultAdapter(ArrayList()) { result, b -> itemClick(result,b) }
        } else {
            ResultAdapter(mainActivity!!.viewModel.categories.value!!) { result, b -> itemClick(result, b) }
        }
        mainActivity!!.viewModel.categories.observe(viewLifecycleOwner, androidx.lifecycle.Observer { categoryList ->
            adapter.categoryList = categoryList
            adapter.notifyDataSetChanged()
        })
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
        viewModel.results.observe(viewLifecycleOwner, androidx.lifecycle.Observer { resultList ->
            adapter.update(resultList)
            bind.time = Utils.getHourMinute(resultList.map { result -> result.time }.sum())
        })
        viewModel.getResults()
        weekCalendar.calendarListener = object : HorizontalCalendarListener() {
            override fun onDateSelected(date: Calendar?, position: Int) {
                dateTime = dateTime.withDate(date!!.get(Calendar.YEAR),date.get(Calendar.MONTH)+1,date.get(Calendar.DAY_OF_MONTH))
                calendarView.date = dateTime.toDate().time
                viewModel.snapshot!!.remove()
                viewModel.dateTime.apply {
                    value = dateTime
                }
                bind.month = DEF_FMT.print(dateTime)
                viewModel.getResults()
            }
        }
        calendarView.setOnDateChangeListener { _, year, month, dayOfMonth ->
            dateTime = dateTime.withDate(year,month+1,dayOfMonth)
            weekCalendar.selectDate(dateTime.toCalendar(Locale.JAPAN),true)
        }
        bind.addButton.setOnClickListener {
            Utils.eventLog(context,"Result","Add")
            val intent = Intent(activity,ResultInputActivity::class.java)
            intent.putExtra(PARAM_RESULT,Result(resultDate = dateTime.toDate()))
            intent.putExtra(PARAM_RESULT_LIST,adapter.resultList)
            intent.putExtra(PARAM_CATEGORY,mainActivity!!.viewModel.categories.value!!)
            startActivity(intent)
        }
        bind.line.setOnClickListener {
            Utils.eventLog(context,"Result","Calender")
            bind.isCalendar = calendarView.visibility == View.GONE
        }
        if(arguments!!.getBoolean("register",false)) {
            Utils.eventLog(context,"Result","Add")
            val intent = Intent(activity,ResultInputActivity::class.java)
            intent.putExtra(PARAM_RESULT,Result(resultDate = dateTime.toDate()))
            intent.putExtra(PARAM_RESULT_LIST,adapter.resultList)
            intent.putExtra(PARAM_CATEGORY,mainActivity!!.viewModel.categories.value!!)
            startActivity(intent)
        }
        val swipeDetector = ActivitySwipeDetector(activity = activity!!, onSwipeListener = object : OnSwipeListener {
            override fun onLeft() {
                dateTime = dateTime.minusDays(1)
                weekCalendar.selectDate(dateTime.toCalendar(Locale.JAPAN),true)
            }

            override fun OnRight() {
                dateTime = dateTime.plusDays(1)
                weekCalendar.selectDate(dateTime.toCalendar(Locale.JAPAN),true)
            }
        })
        recyclerView.setOnTouchListener(swipeDetector)
        if(mainActivity == null) return bind.root
//        if(mainActivity!!.viewModel.user.value != null && mainActivity!!.viewModel.user.value!!.isPurchaseState) {
//            bind.adView.visibility = View.GONE
//        }
//
//        mainActivity!!.viewModel.user.observe(viewLifecycleOwner, androidx.lifecycle.Observer { user ->
//            if(user != null && user.isPurchaseState) {
//                bind.adView.visibility = View.GONE
//            }
//        })
        return bind.root
    }

    private fun itemClick(result:Result, isLong:Boolean) {
        if(isLong) {
            Utils.eventLog(context,"Result","Delete")
            // ダイアログを作成して表示
            AlertDialog.Builder(context!!).apply {
                setTitle(R.string.title_delete)
                setPositiveButton("OK") { _, _ ->
                    // OKをタップしたときの処理
                    viewModel.deleteResult(result)
                    Toast.makeText(context, R.string.deleted, Toast.LENGTH_LONG).show()
                }
                setNegativeButton("Cancel") { _ , _ ->
                    adapter.notifyDataSetChanged()
                }
                show()
            }
        } else {
            Utils.eventLog(context, "Result", "Edit")
            val intent = Intent(activity,ResultInputActivity::class.java)
            intent.putExtra(PARAM_RESULT,result)
            intent.putExtra(PARAM_RESULT_LIST,adapter.resultList)
            intent.putExtra(PARAM_CATEGORY,mainActivity!!.viewModel.categories.value!!)
            startActivity(intent)
        }
    }
}