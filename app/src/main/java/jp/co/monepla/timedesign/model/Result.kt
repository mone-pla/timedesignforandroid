package jp.co.monepla.timedesign.model

import jp.co.monepla.timedesign.common.Model
import jp.co.monepla.timedesign.common.Utils
import org.joda.time.DateTime
import java.io.Serializable
import java.util.Date

data class Result(
        var resultDate: Date = DateTime().withTime(0,0,0,0).toDate(),
        var categoryId: String = "",
        var title:String = "",
        var time: Int = 0,
        var priority:Boolean = false,
        var urgency:Boolean = false,
        var myTime:Boolean = false
):Model(),Serializable {
    fun getHourTime() : String {
        return Utils.getHourMinute(time)
    }
}