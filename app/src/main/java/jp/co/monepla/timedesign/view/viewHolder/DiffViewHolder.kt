package jp.co.monepla.timedesign.view.viewHolder

import android.graphics.Color
import android.util.Log
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.model.Diff


class DiffViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val pie:PieChart = itemView.findViewById(R.id.chart_circle)
    private val bar:BarChart = itemView.findViewById(R.id.chart_bar)
    private val title:AppCompatTextView = itemView.findViewById(R.id.title)

    fun bind(diff: Diff,position:Int) {
        if(position == 0)setupPieChartView(diff)
        else setupBarChartView(diff)
    }

    private fun setupPieChartView(diff: Diff) {
        bar.visibility = View.GONE
        pie.setUsePercentValues(true)

        val desc = Description()
        desc.text = "カテゴリ別実績時間配分"
        title.text = "カテゴリ別実績時間配分"

        pie.description = desc

        val legend: Legend? = pie.legend
        legend?.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT

        /**
         * サンプルデータ
         */

        val sum = diff.resultList.stream().mapToInt { result -> result.time }.sum()
        val entry = ArrayList<PieEntry>()
        val colors:MutableList<Int> = ArrayList()


        diff.resultList.groupBy { result -> result.categoryId }.forEach { (k, v) ->
            val category = diff.categoryList.stream().filter { c -> c.id == k }.findFirst().orElse(null)
            entry.add(PieEntry(v.stream().mapToInt { r -> r.time}.sum().toFloat()/sum, category?.name
                    ?: ""))
            try {
                colors.add(Color.parseColor("#" + category.color))
            }catch (e:java.lang.NumberFormatException) {
                e.printStackTrace()
            }
        }

        /**
         * ラベル
         */
        val dataSet = PieDataSet(entry, "チャートのラベル")
        dataSet.colors = colors
        dataSet.setDrawValues(true)

        val pieData = PieData(dataSet)
        pieData.setValueFormatter(PercentFormatter())
        pieData.setValueTextSize(20f)
        pieData.setValueTextColor(Color.WHITE)

        pie.data = pieData
        pie.invalidate()
    }

    private fun setupBarChartView(diff: Diff) {
        pie.visibility = View.GONE
        bar.data = BarData(getBarData(diff))
        bar.description.text = "予実（%）"
        title.text = "予実（%）"
        bar.description.setPosition(0F,0F)

        //Y軸(左)の設定
        bar.axisLeft.apply {
            axisMinimum = 0f
            axisMaximum = 100f
            labelCount = 5
            setDrawTopYLabelEntry(true)

        }

        //Y軸(右)の設定
        bar.axisRight.apply {
            setDrawLabels(false)
            setDrawGridLines(false)
            setDrawZeroLine(false)
            setDrawTopYLabelEntry(true)
        }

        //X軸の設定
        val labels = diff.categoryList.map { category -> category.name }
        bar.xAxis.apply {
            valueFormatter = IndexAxisValueFormatter(labels)
            labelCount = labels.size //表示させるラベル数
            position = XAxis.XAxisPosition.BOTTOM
            setDrawLabels(true)
            setDrawGridLines(false)
            setDrawAxisLine(true)
        }

        bar.invalidate()
    }

    private fun getBarData(diff: Diff): ArrayList<IBarDataSet> {
        //表示させるデータ
        val planEntry = ArrayList<BarEntry>()
        val colors:MutableList<Int> = ArrayList()
        var i = 0F
        diff.categoryList.forEach { category ->
            Log.e(category.name,(diff.planList.stream().filter { p -> p.categoryId == category.id }.mapToInt { p -> p.time }.sum().toFloat()/60).toString())
            val plan = diff.planList.stream().filter { p -> p.categoryId == category.id }.mapToInt { p -> p.time }.sum().toFloat()/60
            val result = diff.resultList.stream().filter { p -> p.categoryId == category.id }.mapToInt { p -> p.time }.sum().toFloat()/60
            planEntry.add(BarEntry(i,result * 100 / plan))
            try {
                colors.add(Color.parseColor("#" + category.color))
            } catch (e:NumberFormatException) {
                e.printStackTrace()
            }
            i++
        }

        val dataSet = BarDataSet(planEntry, "カテゴリ別予実（%）").apply {
            //ハイライトさせない
            isHighlightEnabled = false
            //Barの色をセット
            setColors(colors)
        }

        val bars = ArrayList<IBarDataSet>()
        bars.add(dataSet)
        return bars
    }
}
