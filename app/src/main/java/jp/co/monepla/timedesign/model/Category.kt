package jp.co.monepla.timedesign.model

import android.graphics.Color
import jp.co.monepla.timedesign.common.Model
import java.io.Serializable
import java.lang.Exception

data class Category(
    var name: String = "",
    var color: String = ""
): Model(),Serializable {
    fun getBackground() : Int {
        return try {
            Color.parseColor("#$color")
        } catch (e:Exception) {
            Color.WHITE
        }
    }
}