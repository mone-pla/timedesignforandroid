package jp.co.monepla.timedesign.common

import android.text.TextUtils.*
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import kotlinx.coroutines.CancellationException
import java.lang.Exception
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

abstract class FirebaseDatabaseRepository<M:Model> {
    private var firestore = FirebaseFirestore.getInstance()
    private var user = FirebaseAuth.getInstance().currentUser

    protected abstract val rootNode: String
    protected abstract var id: String

    val userDocument:DocumentReference
        get() = firestore.collection("users").document(user!!.uid)
    // get saved addresses from firebase
    val saved: CollectionReference
        get() = userDocument.collection(rootNode)

    fun saveItem(model: M,onSaveListener: OnSaveListener) {
        val collectionReference = userDocument.collection(rootNode)
        if(isEmpty(model.id)) {
            collectionReference.add(model).addOnCompleteListener { task ->
                onSaveListener.onSave( task.exception )
            }
        } else{
            collectionReference.document(model.id).set(model).addOnCompleteListener { task ->
                onSaveListener.onSave( task.exception )
            }
        }
    }

    fun createdItm(model: M): Task<DocumentReference> {
        val collectionReference = userDocument.collection(rootNode)
        return collectionReference.add(model)
    }

    // save address to firebase
    fun updatedItem(model: M): Task<Void> {
        //var
        val documentReference = userDocument.collection(rootNode).document(model.id)
        return documentReference.set(model)
    }

    fun delete(model: M): Task<Void> {
        val documentReference = userDocument.collection(rootNode)
                .document(model.id)
        return documentReference.delete()
    }

    fun getNonEventSaved(categoryId: String): Query {
        return userDocument.collection(rootNode).whereEqualTo("categoryId", categoryId)
    }


}

interface OnSaveListener {
    fun onSave(exception: Exception?)
}