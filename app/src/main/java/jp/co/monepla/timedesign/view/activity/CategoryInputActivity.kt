package jp.co.monepla.timedesign.view.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.gms.ads.AdRequest
import com.mixpanel.android.mpmetrics.MixpanelAPI
import de.hdodenhof.circleimageview.CircleImageView
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.Const
import jp.co.monepla.timedesign.common.Const.PARAM_CATEGORY
import jp.co.monepla.timedesign.common.OnSaveListener
import jp.co.monepla.timedesign.common.Utils
import jp.co.monepla.timedesign.databinding.ActivityCategoryInputBinding
import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.model.Plan
import jp.co.monepla.timedesign.repository.CategoryRepository
import jp.co.monepla.timedesign.view.fragment.dialog.ColorPickerDialog
import jp.co.monepla.timedesign.viewModel.CategoryAddViewModel

class CategoryInputActivity :AppCompatActivity() {
    private var colorCircleViewList:Map<Int, CircleImageView> = mutableMapOf()
    private var color: Int = R.id.color1
    private var category = Category()
    private val repository = CategoryRepository()
    private lateinit var colorPickerDialog:ColorPickerDialog
    private lateinit var bind: ActivityCategoryInputBinding
    private lateinit var mixPanelAPI: MixpanelAPI
    private val colorMap:Map<Int,Int> = hashMapOf(
            R.id.color1 to R.color.soft1,
            R.id.color2 to R.color.soft2,
            R.id.color3 to R.color.soft3,
            R.id.color4 to R.color.soft4,
            R.id.color5 to R.color.soft5,
            R.id.color6 to R.color.soft6,
            R.id.color7 to R.color.soft7,
            R.id.color8 to R.color.soft8,
            R.id.color9 to R.color.soft9,
            R.id.color10 to R.color.soft10,
            R.id.color11 to R.color.soft11,
            R.id.color12 to R.color.soft12
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind = DataBindingUtil.setContentView(this, R.layout.activity_category_input)
        bind.lifecycleOwner = this
        bind.viewModel = CategoryAddViewModel()
        mixPanelAPI = MixpanelAPI.getInstance(this,Const.MIX_PANEL_TOKEN)
        mixPanelAPI.track("CategoryInputActivity")
        if(!intent.getBooleanExtra(Const.PARAM_NEW,false)) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            val adRequest = AdRequest.Builder().build()
            bind.adView.loadAd(adRequest)
            bind.adView.visibility = View.VISIBLE
        }
        else bind.firstAction.visibility = View.VISIBLE
        supportActionBar!!.title = getString(R.string.title_dialog_category)
        bind.positiveButton.setOnClickListener {
            save()
        }
        colorPickerDialog = ColorPickerDialog(dataSet)
        bind.colorPicker.setOnClickListener {
            mixPanelAPI.track("CategoryInputActivity", Utils.getJSON("Category","ColorPicker"))
            colorPickerDialog.show(supportFragmentManager,"")
        }
        setColorEvent()
        category = if(intent.getSerializableExtra(PARAM_CATEGORY) == null) Category() else intent.getSerializableExtra(PARAM_CATEGORY) as Category
        bind.viewModel!!.categoryName.postValue(category.name)
        if(!TextUtils.isEmpty(category.color)) {
            colorMap.forEach { (key, value) ->
                if(Integer.toHexString(getColor(value)) == category.color) {
                    setColor(key)
                }
            }
            bind.viewModel!!.color.postValue(category.color)
        }
        bind.colorCode.setOnFocusChangeListener { _, hasFocus ->
            mixPanelAPI.track("CategoryInputActivity", Utils.getJSON("Category","ColorCode"))
            if(!hasFocus) {
                setColor(-1)
            }
        }
    }

    /**
     * Called when the activity has detected the user's press of the back
     * key. The [OnBackPressedDispatcher][.getOnBackPressedDispatcher] will be given a
     * chance to handle the back button before the default behavior of
     * [android.app.Activity.onBackPressed] is invoked.
     *
     * @see .getOnBackPressedDispatcher
     */
    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     *
     * Derived classes should call through to the base class for it to
     * perform the default menu handling.
     *
     * @param item The menu item that was selected.
     *
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     *
     * @see .onCreateOptionsMenu
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) { finish() }
        return super.onOptionsItemSelected(item)
    }

    private fun setColorEvent() {

        colorCircleViewList = mutableMapOf(
                R.id.color1 to bind.grid.findViewById(R.id.color1),
                R.id.color2 to bind.grid.findViewById(R.id.color2),
                R.id.color3 to bind.grid.findViewById(R.id.color3),
                R.id.color4 to bind.grid.findViewById(R.id.color4),
                R.id.color5 to bind.grid.findViewById(R.id.color5),
                R.id.color6 to bind.grid.findViewById(R.id.color6),
                R.id.color7 to bind.grid.findViewById(R.id.color7),
                R.id.color8 to bind.grid.findViewById(R.id.color8),
                R.id.color9 to bind.grid.findViewById(R.id.color9),
                R.id.color10 to bind.grid.findViewById(R.id.color10),
                R.id.color11 to bind.grid.findViewById(R.id.color11),
                R.id.color12 to bind.grid.findViewById(R.id.color12)
        )

        colorCircleViewList.forEach {(_,value) ->
            value.setOnClickListener {view -> setColor(view.id) }
        }
    }

    private fun setColor(id:Int) {
        mixPanelAPI.track("CategoryInputActivity", Utils.getJSON("Category","setColor"))
        colorCircleViewList.forEach { (key, value) ->
            if(key == id) {
                value.borderColor = getColor(android.R.color.black)
                color = id
                if(colorMap.containsKey(key)) bind.viewModel!!.color.postValue(Integer.toHexString(getColor(colorMap[key] ?: 0)))
            } else {
                value.borderColor = getColor(android.R.color.transparent)
            }
        }
    }

    private fun save() {
        category.name = bind.viewModel!!.categoryName.value.orEmpty()
        category.color = bind.viewModel!!.color.value.orEmpty()
        repository.saveItem(category, object : OnSaveListener {
            override fun onSave(exception: Exception?) {
                mixPanelAPI.track("CategoryInputActivity", Utils.getJSON("Category","save"))
                if(exception != null) Toast.makeText(this@CategoryInputActivity,"", Toast.LENGTH_SHORT).show()
                else if(!intent.getBooleanExtra(Const.PARAM_NEW,false))finish()
                val planIntent = Intent(this@CategoryInputActivity,PlanInputActivity::class.java)
                planIntent.putExtra(Const.PARAM_NEW,true)
                planIntent.putExtra(Const.PARAM_PLAN, Plan())
                startActivity(planIntent)
                finish()
            }
        })
    }

    private val dataSet = object: ColorPickerDialog.ColorPickerListener {
        override fun onColorSet(color:Int) {
            bind.viewModel!!.color.postValue(Integer.toHexString(color))
            setColor(-1)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mixPanelAPI.flush()
    }
}