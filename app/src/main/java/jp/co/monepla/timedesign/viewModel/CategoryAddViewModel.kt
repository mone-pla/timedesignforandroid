package jp.co.monepla.timedesign.viewModel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import jp.co.monepla.timedesign.common.Utils

class CategoryAddViewModel:ViewModel() {
    var categoryName: MutableLiveData<String> = MutableLiveData()
    var color: MutableLiveData<String> = MutableLiveData()
    var isButtonEnabled = MediatorLiveData<Boolean>()

    init {
        val observer = Observer<String> {
            isValid()
        }

        isButtonEnabled.addSource(categoryName,observer)
        isButtonEnabled.addSource(color,observer)
    }
    private fun isValid() {
      isButtonEnabled.postValue(!categoryName.value.isNullOrBlank() && isValidColor())
    }

    private fun isValidColor():Boolean {
        if(color.value.isNullOrBlank()) return false
        return Utils.isColor(color.value!!)
    }
}