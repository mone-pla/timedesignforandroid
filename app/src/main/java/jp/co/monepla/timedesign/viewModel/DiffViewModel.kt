package jp.co.monepla.timedesign.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import java.util.ArrayList

import jp.co.monepla.timedesign.model.Plan
import jp.co.monepla.timedesign.model.Result
import jp.co.monepla.timedesign.repository.PlanRepository
import jp.co.monepla.timedesign.repository.ResultRepository

class DiffViewModel : ViewModel() {
    private var plans: MutableLiveData<List<Plan>> = MutableLiveData()
    private val planRepository = PlanRepository()
    private var results: MutableLiveData<List<Result>> = MutableLiveData()
    private val resultRepository = ResultRepository()


    fun loadPlans(): LiveData<List<Plan>> {
        planRepository.saved.addSnapshotListener { queryDocumentSnapshots, e ->
            if (e != null || queryDocumentSnapshots == null) {
                plans = MutableLiveData()
                return@addSnapshotListener
            }
            val planList = ArrayList<Plan>()
            for (doc in queryDocumentSnapshots.documents) {
                val plan = doc.toObject(Plan::class.java)
                planList.add(plan!!)
            }
            plans.setValue(planList)
        }
        return plans
    }


    fun getResults(): LiveData<List<Result>> {
        resultRepository.saved.addSnapshotListener { queryDocumentSnapshots, e ->
            if (e != null || queryDocumentSnapshots == null) {
                results = MutableLiveData()
                return@addSnapshotListener
            }
            val resultList = ArrayList<Result>()
            for (doc in queryDocumentSnapshots.documents) {
                val result = doc.toObject(Result::class.java)
                resultList.add(result!!)
            }
            results.setValue(resultList)
        }
        return results
    }

}
