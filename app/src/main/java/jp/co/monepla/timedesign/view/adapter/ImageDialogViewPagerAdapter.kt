package jp.co.monepla.timedesign.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.view.fragment.PageFragment

class ImageDialogViewPagerAdapter internal constructor(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private val images = arrayOf(
            R.drawable.snap_category,
            R.drawable.snap_category_dialog,
            R.drawable.snap_plan,
            R.drawable.snap_plan_dialog,
            R.drawable.snap_result,
            R.drawable.snap_result2,
            R.drawable.snap_result_dialog,
            R.drawable.snap_diff,
            R.drawable.snap_diff2)


    /**
     * Return the Fragment associated with a specified position.
     */
    override fun getItem(position: Int): Fragment {
        return PageFragment.newInstance(images[position])
    }

    /**
     * Return the number of views available.
     */
    override fun getCount(): Int {
        return images.size
    }

}