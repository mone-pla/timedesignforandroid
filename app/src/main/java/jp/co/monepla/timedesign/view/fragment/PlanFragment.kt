package jp.co.monepla.timedesign.view.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast.LENGTH_LONG
import android.widget.Toast.makeText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import jp.co.monepla.timedesign.R
import jp.co.monepla.timedesign.common.*
import jp.co.monepla.timedesign.common.Const.PARAM_CATEGORY
import jp.co.monepla.timedesign.common.Const.PARAM_PLAN
import jp.co.monepla.timedesign.common.Const.PARAM_PLAN_LIST
import jp.co.monepla.timedesign.model.Plan
import jp.co.monepla.timedesign.view.activity.PlanInputActivity
import jp.co.monepla.timedesign.view.adapter.PlanAdapter
import jp.co.monepla.timedesign.viewModel.PlanListViewModel
import org.joda.time.DateTime

class PlanFragment : AbstractFragment() {

    private var recyclerView: RecyclerView? = null
    private var planMonth: AppCompatTextView? = null
    private var preMonth: AppCompatImageButton? = null
    private var afterMonth: AppCompatImageButton? = null
    private var restTime: AppCompatTextView? = null
    private var totalRestTime: AppCompatTextView? = null
    private var planList = ArrayList<Plan>()
    private var planAdapter: PlanAdapter? = null
    private var planListViewModel: PlanListViewModel? = null
    private var dateTime: DateTime = DateTime().withTime(0,0,0,0)


    /**
     * Called when the fragment's activity has been created and this
     * fragment's view hierarchy instantiated.  It can be used to do final
     * initialization once these pieces are in place, such as retrieving
     * views or restoring state.  It is also useful for fragments that use
     * [.setRetainInstance] to retain their instance,
     * as this leftCallback tells the fragment when it is fully associated with
     * the new activity instance.  This is called after [.onCreateView]
     * and before [.onViewStateRestored].
     *
     * @param savedInstanceState If the fragment is being re-created from
     * a previous saved state, this is the state.
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        planListViewModel = ViewModelProviders.of(this).get(PlanListViewModel::class.java)
        planListViewModel!!.loadPlans()!!.observe(this, Observer{ plans ->
            planList = plans
            setAdapterData()
            setHeaderData()
        })
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * [.onCreate] and [.onActivityCreated].
     *
     *
     * If you return a View from here, you will later be called in
     * [.onDestroyView] when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     * any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     * UI should be attached to.  The fragment should not add the view itself,
     * but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     * from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        Utils.eventLog(context,"Plan","Load")
        val view = inflater.inflate(R.layout.fragment_plan_list, container, false)
        setLayoutBind(view)
        setLayoutEvent(view)
        setLayoutData()
        return view
    }

    private fun setLayoutBind(view: View) {
        recyclerView = view.findViewById(R.id.plan_recycler)
        planMonth = view.findViewById(R.id.plan_month)
        preMonth = view.findViewById(R.id.pre_month)
        afterMonth = view.findViewById(R.id.after_month)
        restTime = view.findViewById(R.id.rest_time)
        totalRestTime = view.findViewById(R.id.total_rest_time)
        val adView = view.findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }
    @SuppressLint("ClickableViewAccessibility")
    private fun setLayoutEvent(view: View) {
        view.findViewById<View>(R.id.add_button).setOnClickListener {
            Utils.eventLog(context,"Plan","Add")
            val intent = Intent(activity,PlanInputActivity::class.java)
            intent.putExtra(PARAM_PLAN,Plan(planMonth = dateTime.toDate()))
            intent.putExtra(PARAM_PLAN_LIST,planAdapter!!.planList)
            intent.putExtra(PARAM_CATEGORY,mainActivity!!.viewModel.categories.value!!)
            startActivity(intent)
        }
        preMonth!!.setOnClickListener {
            dateTime = dateTime.minusMonths(1)
            setAdapterData()
            setHeaderData()
            Utils.eventLog(context,"Plan","Pre")
        }

        afterMonth!!.setOnClickListener {
            Utils.eventLog(context,"Plan","Next")
            dateTime = dateTime.plusMonths(1)
            setAdapterData()
            setHeaderData()
        }
        val swipeDetector = ActivitySwipeDetector(activity = activity!!, onSwipeListener = object : OnSwipeListener {
            override fun onLeft() {
                dateTime = dateTime.minusMonths(1)
                setAdapterData()
                setHeaderData()
                Utils.eventLog(context,"Plan","Pre")
            }

            override fun OnRight() {
                Utils.eventLog(context,"Plan","Next")
                dateTime = dateTime.plusMonths(1)
                setAdapterData()
                setHeaderData()
            }
        })
        recyclerView!!.setOnTouchListener(swipeDetector)
        if(mainActivity == null) return
//        if(mainActivity!!.viewModel.user.value != null && mainActivity!!.viewModel.user.value!!.isPurchaseState) {
//            view.findViewById<AdView>(R.id.adView).visibility = View.GONE
//        }

//        mainActivity!!.viewModel.user.observe(this, Observer { user ->
//            if(user != null && user.isPurchaseState) {
//                view.findViewById<AdView>(R.id.adView).visibility = View.GONE
//            }
//        })
    }


    private fun setLayoutData() {
        planAdapter = PlanAdapter(context!!) {plan, isLong ->
            if(isLong) {
                Utils.eventLog(context,"Plan","Delete")
                // ダイアログを作成して表示
                AlertDialog.Builder(context!!).apply {
                    setTitle(R.string.title_delete)
                    setPositiveButton("OK") { _, _ ->
                        // OKをタップしたときの処理
                        planListViewModel!!.deletePlan(plan)
                        makeText(context, R.string.deleted, LENGTH_LONG).show()
                    }
                    setNegativeButton("Cancel") { _, _ ->
                        planAdapter!!.notifyDataSetChanged()
                    }
                    show()
                }
            } else {
                Utils.eventLog(context,"Plan","Edit")
                val intent = Intent(activity,PlanInputActivity::class.java)
                intent.putExtra(PARAM_PLAN,plan)
                intent.putExtra(PARAM_PLAN_LIST,planAdapter!!.planList)
                intent.putExtra(PARAM_CATEGORY,mainActivity!!.viewModel.categories.value!!)
                startActivity(intent)
            }
        }
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = planAdapter
        dateTime = dateTime.withDayOfMonth(1)
        setHeaderData()
    }

    private fun setAdapterData() {
        planAdapter!!.setData(planList
                .filter { plan ->
                    plan.planMonth.time == dateTime.toDate().time
                }.sortedBy { plan ->
                    plan.categoryId
                }.toCollection(arrayListOf())
        )
        view!!.invalidate()
    }

    private fun setHeaderData() {
        val planMonthStr:String = dateTime.year.toString() + getString(R.string.slash) + dateTime.monthOfYear.toString()
        planMonth!!.text = planMonthStr
        totalRestTime!!.text = Utils.getHourMinute(Const.DAY_TINE * dateTime.dayOfMonth().maximumValue)
        val restTimeStr:String = Utils.getHourMinute(
                planAdapter!!.planList.map { plan ->
                    plan.time
                }.sum()
        )
        restTime!!.text = restTimeStr
    }
}
