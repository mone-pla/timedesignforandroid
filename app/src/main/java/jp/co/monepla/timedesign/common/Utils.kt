package jp.co.monepla.timedesign.common

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.android.billingclient.api.Purchase
import com.google.android.gms.tasks.Task
import com.google.firebase.analytics.FirebaseAnalytics
import jp.co.monepla.timedesign.model.User
import jp.co.monepla.timedesign.receiver.Notifier
import jp.co.monepla.timedesign.viewModel.MainViewModel
import jp.co.recruit_mp.android.rmp_appiraterkit.AppiraterDialogBuilder
import jp.co.recruit_mp.android.rmp_appiraterkit.AppiraterUtils
import kotlinx.coroutines.CancellationException
import org.joda.time.DateTime
import org.json.JSONObject
import java.util.*
import java.util.regex.Pattern
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


class Utils {
    companion object {
        fun getHourMinute(progress:Int):String {
            return (progress/60).toString() + ":" + (progress%60)
        }
        fun isEmailValid(email: String): Boolean {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        fun eventLog(context: Context?,id:String,name:String) {
            if(context == null) return
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)
            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id)
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name)
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image")
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
        }

        fun parseInt(text:String) :Int {
            return if(!text.isBlank()) Integer.parseInt(text) else 0
        }

        fun setAlert(context: Context) {
            val notificationIntent = Intent(context, Notifier::class.java)
            notificationIntent.putExtra(Notifier.NOTIFICATION_ID, 1)
            notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            val pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(pendingIntent)
            var dateTime : DateTime = DateTime().withTime(22,0,0,0)
            if(dateTime.isBeforeNow) {
                dateTime = dateTime.plusDays(1)
            }

//            if(Debug.isDebuggerConnected()){
//                dateTime = DateTime.now().plusMinutes(5)
//            }
            Log.e("Alert",Date(dateTime.toDate().time - DateTime.now().toDate().time).toString())
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,dateTime.toDate().time - DateTime.now().toDate().time,86400000,pendingIntent)
        }

        fun setAutoImportCalendar(context: Context) {
            val notificationIntent = Intent(context, Notifier::class.java)
            notificationIntent.putExtra(Notifier.NOTIFICATION_ID, 2)
            notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            val pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(pendingIntent)
            var dateTime : DateTime = DateTime().withTime(8,0,0,0)
            if(dateTime.isBeforeNow) {
                dateTime = dateTime.plusDays(1)
            }

//            if(Debug.isDebuggerConnected()){
//                dateTime = DateTime.now().plusMinutes(5)
//            }
            Log.e("Alert",Date(dateTime.toDate().time - DateTime.now().toDate().time).toString())
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,dateTime.toDate().time - DateTime.now().toDate().time,86400000,pendingIntent)
        }

        fun removeCalendar(context: Context) {
            val notificationIntent = Intent(context, Notifier::class.java)
            notificationIntent.putExtra(Notifier.NOTIFICATION_ID, 2)
            notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            val pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(pendingIntent)
        }

        fun updateUser(viewModel: MainViewModel, status:Int) {
            if(status >= 0 && viewModel.user.value != null)
                viewModel.updateUser(User(status == Purchase.PurchaseState.PURCHASED))
        }

        fun isColor(color:String) :Boolean {
            val regex = "^([0-9a-fA-F]{3}|[0-9a-fA-F]{6}|[0-9a-fA-F]{8})\$"
            val p: Pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE)
            return p.matcher(color).find()
        }

        fun launchStore(context: Context) {
            // レビューダイアログを生成する
            val builder = AppiraterDialogBuilder(context)
            builder
                    // ダイアログのタイトル
                    .setTitle("いつも使ってくれてありがとう!")
                    // ダイアログのメッセージ
                    .setMessage("よかったら評価をお願いします！")
                    // ストアのアプリページに遷移するボタン
                    .addButton("★5をつける") { dialog ->
                        eventLog(context,"Main","review")
                        AppiraterUtils.launchStore(context)
                        dialog!!.dismiss()
                    }
            val dialog = builder.create()
            dialog.show()
        }

        fun getJSON(vararg params: String):JSONObject {
            val obj = JSONObject()
            for (i in 0 until params.size/2) {
                obj.put(params[i*2],params[i*2+1])
            }
            return obj
        }

    }

}