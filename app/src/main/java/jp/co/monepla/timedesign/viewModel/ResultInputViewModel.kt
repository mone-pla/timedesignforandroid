package jp.co.monepla.timedesign.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.ListenerRegistration
import jp.co.monepla.timedesign.common.OnSaveListener
import jp.co.monepla.timedesign.model.Category
import jp.co.monepla.timedesign.model.Result
import jp.co.monepla.timedesign.repository.CategoryRepository
import jp.co.monepla.timedesign.repository.ResultRepository
import org.joda.time.DateTime
import java.lang.Exception

class ResultInputViewModel :ViewModel() {
    val repository:ResultRepository = ResultRepository()
    var results: MutableLiveData<ArrayList<Result>> = MutableLiveData()
    var snapshot: ListenerRegistration? = null
    var categories:MutableLiveData<ArrayList<Category>> = MutableLiveData()
    fun saveItem(model : Result,onSaveListener: OnSaveListener) {
        repository.saveItem(model,onSaveListener)
    }

    fun getResults(dateTime: DateTime) {
        snapshot = repository.saved.whereEqualTo("resultDate",dateTime.toDate())
                .addSnapshotListener { querySnapshot, e ->
                    if (e != null || querySnapshot == null) {
                        return@addSnapshotListener
                    }
                    val resultList = ArrayList<Result>()
                    for (doc in querySnapshot.documents) {
                        val result = doc.toObject(Result::class.java)!!.withId<Result>(doc.id)
                        resultList.add(result)
                    }
                    results.value = resultList.toCollection(arrayListOf())
                }
    }

    fun getCategory() {
        CategoryRepository().saved.addSnapshotListener { querySnapshot, _ ->
            val categoryList = ArrayList<Category>()
            for (doc in querySnapshot!!.documents) {
                categoryList.add(doc.toObject(Category::class.java)!!.withId(doc.id))
            }
            categories.value = categoryList
        }
    }
}